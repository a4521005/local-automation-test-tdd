const puppeteer = require('puppeteer');
const expect = require('chai').expect;

const {click,getText,getCount,shouldExist,waitForText} = require('../../lib/helper')

describe('B2C EN-US Projector - Portable Projectors',()=>{
    let browser
    let page
    //Test Hooks:before, beforeEach, after, afterEach
    //before:每個test case執行之前先做的動作(page/browswer)
    before(async function(){
        browser=await puppeteer.launch({
            // executablePath:
            // "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
            executablePath:
            "./node_modules/puppeteer/.local-chromium/win32-901912/chrome-win/chrome",
            headless:true,//無介面模式:有無需要開視窗,false要開,true不開
            slowMo:100,// slow down by 100ms
            devtools:false//有無需要開啟開發人員工具
        })
        page=await browser.newPage()
        //設定像素
        await page.setViewport({width:1200,height:1000})

        await page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
        await page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    })
    //after:每個test case執行之後統一要做的動作(page/browswer)
    after(async function(){
        await browser.close()
    })
    it('Go to Series Page - Check number(GV Series)',async function(){
        //step1:進去B2C各個Projector Series Page
        await page.goto('https://www.benq.com/en-us/projector/portable.html')
        const gvPortableProjector = " #productlist_copy > div.right > ul.products > ul > li"
        //step2:計算每一個Series Page數量(ex:假設現在是10個, 之後數量如果增加了, 腳本就要改) 
        const countgvPortableProjector = await getCount(page, gvPortableProjector)
        console.log("Total of Portable Projector(GV Series):",countgvPortableProjector)
        expect(countgvPortableProjector).to.equal(2)//目前gvPortableProjector有2個產品
    })
    it('Go to Series Page - Check number(GS Series)',async function(){
        //step1:進去B2C各個Projector Series Page
        await page.goto('https://www.benq.com/en-us/projector/portable.html')
        const gsPortableProjector = " #productlist_copy_cop > div.right > ul.products > ul > li"
        //step2:計算每一個Series Page數量(ex:假設現在是10個, 之後數量如果增加了, 腳本就要改) 
        const countgsPortableProjector = await getCount(page, gsPortableProjector)
        console.log("Total of Portable Projector(GS Series):",countgsPortableProjector)
        expect(countgsPortableProjector).to.equal(2)//目前gsPortableProjector有8個產品
    })
    it('check all Spec exist or not(GV Series)',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/portable.html')
        //第一個projector
        var getgvPortableProjectorURL = " #productlist_copy > div.right > ul.products > ul > li:nth-child(1) > a"
        var gvPortableProjectorProductURL = await page.$eval(getgvPortableProjectorURL , element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var gvPortableProjectorURL  = gvPortableProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var gvPortableProjectorSpecURL = "https://www.benq.com/"+gvPortableProjectorURL
        console.log(gvPortableProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(gvPortableProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })

    it('check all Spec exist or not(GV Series)',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/portable.html')
        //第一個projector
        var getgvPortableProjectorURL = " #productlist_copy > div.right > ul.products > ul > li:nth-child(2) > a"
        var gvPortableProjectorProductURL = await page.$eval(getgvPortableProjectorURL , element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var gvPortableProjectorURL  = gvPortableProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var gvPortableProjectorSpecURL = "https://www.benq.com/"+gvPortableProjectorURL
        console.log(gvPortableProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(gvPortableProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })

    it('check all Spec exist or not(GS Series)',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/portable.html')
        //第一個projector
        var getgsPortableProjectorURL = " #productlist_copy_cop > div.right > ul.products > ul > li:nth-child(1) > a"
        var gsPortableProjectorProductURL = await page.$eval(getgsPortableProjectorURL , element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var gsPortableProjectorURL  = gsPortableProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var gsPortableProjectorSpecURL = "https://www.benq.com/"+gsPortableProjectorURL
        console.log(gsPortableProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(gsPortableProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })

    it('check all Spec exist or not(GS Series)',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/portable.html')
        //第一個projector
        var getgsPortableProjectorURL = " #productlist_copy_cop > div.right > ul.products > ul > li:nth-child(2) > a"
        var gsPortableProjectorProductURL = await page.$eval(getgsPortableProjectorURL , element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var gsPortableProjectorURL  = gsPortableProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var gsPortableProjectorSpecURL = "https://www.benq.com/"+gsPortableProjectorURL
        console.log(gsPortableProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(gsPortableProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
})
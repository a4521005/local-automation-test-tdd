const puppeteer = require('puppeteer');
const expect = require('chai').expect;

const {click,getText,getCount,shouldExist,waitForText} = require('../../lib/helper')

describe('B2C EN-US Projector - Accessories',()=>{
    let browser
    let page
    //Test Hooks:before, beforeEach, after, afterEach
    //before:每個test case執行之前先做的動作(page/browswer)
    before(async function(){
        browser=await puppeteer.launch({
            executablePath:
            "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
            //executablePath:
            //"./node_modules/puppeteer/.local-chromium/win32-901912/chrome-win/chrome",
            headless:true,//無介面模式:有無需要開視窗,false要開,true不開
            slowMo:100,// slow down by 100ms
            devtools:false//有無需要開啟開發人員工具
        })
        page=await browser.newPage()
        //設定像素
        await page.setViewport({width:1200,height:1000})

        await page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
        await page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    })
    //after:每個test case執行之後統一要做的動作(page/browswer)
    after(async function(){
        await browser.close()
    })
    it('Go to Accessories Series Page - Check number',async function(){
        //step1:進去B2C各個Projector Series Page
        await page.goto('https://www.benq.com/en-us/projector/accessory.html')
        const accessoriesProjector = "#seriesproducts_copy > div.right > ul.products > ul > li"
        //step2:計算Accessories數量(ex:假設現在是77個, 之後數量如果增加了, 腳本就要改) 
        const countaccessoriesProjector = await getCount(page, accessoriesProjector)
        console.log("Total of Accessories:",countaccessoriesProjector)
        expect(countaccessoriesProjector).to.equal(77)//目前Accessories有77個產品
    })
    it('check each Spec is null or not',async function(){
        //step3:取得URL
        await page.goto('https://www.benq.com/en-us/projector/accessory/wdrt8192/specifications.html')
        //step4:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    it('check each Spec is null or not',async function(){
        //step3:取得URL
        await page.goto('https://www.benq.com/en-us/projector/accessory/video-streaming-dongle-qcast-qp01/specifications.html')
        //step4:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    it('check each Spec is null or not',async function(){
        //step3:取得URL
        await page.goto('https://www.benq.com/en-us/projector/accessory/wireless-fhd-kit-wdp02/specifications.html')
        //step4:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    it('check each Spec is null or not',async function(){
        //step3:取得URL
        await page.goto('https://www.benq.com/en-us/projector/accessory/qcast-mirror-qp20/specifications.html')
        //step4:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
})
describe('B2C EN-US Projector - Business Projectors',()=>{
    let browser
    let page
    //Test Hooks:before, beforeEach, after, afterEach
    //before:每個test case執行之前先做的動作(page/browswer)
    before(async function(){
        browser=await puppeteer.launch({
            executablePath:
            "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
            // executablePath:
            // "./node_modules/puppeteer/.local-chromium/win32-901912/chrome-win/chrome",
            headless:true,//無介面模式:有無需要開視窗,false要開,true不開
            slowMo:100,// slow down by 100ms
            devtools:false//有無需要開啟開發人員工具
        })
        page=await browser.newPage()
        //設定像素
        await page.setViewport({width:1200,height:1000})

        await page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
        await page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    })
    //after:每個test case執行之後統一要做的動作(page/browswer)
    after(async function(){
        await browser.close()
    })
    it('Go to Business Projectors Series Page - Meeting Room Projector - Check number',async function(){
        await page.goto('https://www.benq.com/en-us/projector/meeting-room.html')
        const businessMeetingRoomProjector = "#seriesproducts > div.right > ul.products > ul > li"
        //step2:計算Business Projector - Meeting Room Projector數量(ex:假設現在是12個, 之後數量如果增加了, 腳本就要改) 
        const countbusinessMeetingRoomProjector = await getCount(page, businessMeetingRoomProjector)
        console.log("Total of Business Projector - Meeting Room Projector:",countbusinessMeetingRoomProjector)
        expect(countbusinessMeetingRoomProjector).to.equal(12)//目前Accessories有77個產品
    })
    it('check each Spec is null or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/meeting-room.html')
        var getBusinessMeetingRoomProjectorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(1) > a"
        var businessMeetingRoomProjectorProductURL = await page.$eval(getBusinessMeetingRoomProjectorURL, element=> element.getAttribute("href"))
        // console.log(businessMeetingRoomProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var businessMeetingRoomProjectorURL = businessMeetingRoomProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var businessMeetingRoomProjectorSpecURL = "https://www.benq.com/"+businessMeetingRoomProjectorURL
        console.log(businessMeetingRoomProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(businessMeetingRoomProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    it('check each Spec is null or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/meeting-room.html')
        var getBusinessMeetingRoomProjectorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(2) > a"
        var businessMeetingRoomProjectorProductURL = await page.$eval(getBusinessMeetingRoomProjectorURL, element=> element.getAttribute("href"))
        // console.log(businessMeetingRoomProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var businessMeetingRoomProjectorURL = businessMeetingRoomProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var businessMeetingRoomProjectorSpecURL = "https://www.benq.com/"+businessMeetingRoomProjectorURL
        console.log(businessMeetingRoomProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(businessMeetingRoomProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    it('check each Spec is null or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/meeting-room.html')
        var getBusinessMeetingRoomProjectorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(3) > a"
        var businessMeetingRoomProjectorProductURL = await page.$eval(getBusinessMeetingRoomProjectorURL, element=> element.getAttribute("href"))
        // console.log(businessMeetingRoomProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var businessMeetingRoomProjectorURL = businessMeetingRoomProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var businessMeetingRoomProjectorSpecURL = "https://www.benq.com/"+businessMeetingRoomProjectorURL
        console.log(businessMeetingRoomProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(businessMeetingRoomProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    it('check each Spec is null or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/meeting-room.html')
        var getBusinessMeetingRoomProjectorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(4) > a"
        var businessMeetingRoomProjectorProductURL = await page.$eval(getBusinessMeetingRoomProjectorURL, element=> element.getAttribute("href"))
        // console.log(businessMeetingRoomProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var businessMeetingRoomProjectorURL = businessMeetingRoomProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var businessMeetingRoomProjectorSpecURL = "https://www.benq.com/"+businessMeetingRoomProjectorURL
        console.log(businessMeetingRoomProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(businessMeetingRoomProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    it('check each Spec is null or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/meeting-room.html')
        var getBusinessMeetingRoomProjectorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(5) > a"
        var businessMeetingRoomProjectorProductURL = await page.$eval(getBusinessMeetingRoomProjectorURL, element=> element.getAttribute("href"))
        // console.log(businessMeetingRoomProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var businessMeetingRoomProjectorURL = businessMeetingRoomProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var businessMeetingRoomProjectorSpecURL = "https://www.benq.com/"+businessMeetingRoomProjectorURL
        console.log(businessMeetingRoomProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(businessMeetingRoomProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    it('check each Spec is null or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/meeting-room.html')
        var getBusinessMeetingRoomProjectorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(6) > a"
        var businessMeetingRoomProjectorProductURL = await page.$eval(getBusinessMeetingRoomProjectorURL, element=> element.getAttribute("href"))
        // console.log(businessMeetingRoomProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var businessMeetingRoomProjectorURL = businessMeetingRoomProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var businessMeetingRoomProjectorSpecURL = "https://www.benq.com/"+businessMeetingRoomProjectorURL
        console.log(businessMeetingRoomProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(businessMeetingRoomProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    it('check each Spec is null or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/meeting-room.html')
        var getBusinessMeetingRoomProjectorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(7) > a"
        var businessMeetingRoomProjectorProductURL = await page.$eval(getBusinessMeetingRoomProjectorURL, element=> element.getAttribute("href"))
        // console.log(businessMeetingRoomProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var businessMeetingRoomProjectorURL = businessMeetingRoomProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var businessMeetingRoomProjectorSpecURL = "https://www.benq.com/"+businessMeetingRoomProjectorURL
        console.log(businessMeetingRoomProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(businessMeetingRoomProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    it('check each Spec is null or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/meeting-room.html')
        var getBusinessMeetingRoomProjectorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(8) > a"
        var businessMeetingRoomProjectorProductURL = await page.$eval(getBusinessMeetingRoomProjectorURL, element=> element.getAttribute("href"))
        // console.log(businessMeetingRoomProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var businessMeetingRoomProjectorURL = businessMeetingRoomProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var businessMeetingRoomProjectorSpecURL = "https://www.benq.com/"+businessMeetingRoomProjectorURL
        console.log(businessMeetingRoomProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(businessMeetingRoomProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    it('check each Spec is null or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/meeting-room.html')
        var getBusinessMeetingRoomProjectorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(9) > a"
        var businessMeetingRoomProjectorProductURL = await page.$eval(getBusinessMeetingRoomProjectorURL, element=> element.getAttribute("href"))
        // console.log(businessMeetingRoomProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var businessMeetingRoomProjectorURL = businessMeetingRoomProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var businessMeetingRoomProjectorSpecURL = "https://www.benq.com/"+businessMeetingRoomProjectorURL
        console.log(businessMeetingRoomProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(businessMeetingRoomProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    it('check each Spec is null or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/meeting-room.html')
        var getBusinessMeetingRoomProjectorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(10) > a"
        var businessMeetingRoomProjectorProductURL = await page.$eval(getBusinessMeetingRoomProjectorURL, element=> element.getAttribute("href"))
        // console.log(businessMeetingRoomProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var businessMeetingRoomProjectorURL = businessMeetingRoomProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var businessMeetingRoomProjectorSpecURL = "https://www.benq.com/"+businessMeetingRoomProjectorURL
        console.log(businessMeetingRoomProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(businessMeetingRoomProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    it('check each Spec is null or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/meeting-room.html')
        var getBusinessMeetingRoomProjectorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(11) > a"
        var businessMeetingRoomProjectorProductURL = await page.$eval(getBusinessMeetingRoomProjectorURL, element=> element.getAttribute("href"))
        // console.log(businessMeetingRoomProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var businessMeetingRoomProjectorURL = businessMeetingRoomProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var businessMeetingRoomProjectorSpecURL = "https://www.benq.com/"+businessMeetingRoomProjectorURL
        console.log(businessMeetingRoomProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(businessMeetingRoomProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    it('check each Spec is null or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/meeting-room.html')
        var getBusinessMeetingRoomProjectorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(12) > a"
        var businessMeetingRoomProjectorProductURL = await page.$eval(getBusinessMeetingRoomProjectorURL, element=> element.getAttribute("href"))
        // console.log(businessMeetingRoomProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var businessMeetingRoomProjectorURL = businessMeetingRoomProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var businessMeetingRoomProjectorSpecURL = "https://www.benq.com/"+businessMeetingRoomProjectorURL
        console.log(businessMeetingRoomProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(businessMeetingRoomProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
})
describe('B2C EN-US Projector - Gaming Projectors',()=>{
    let browser
    let page
    //Test Hooks:before, beforeEach, after, afterEach
    //before:每個test case執行之前先做的動作(page/browswer)
    before(async function(){
        browser=await puppeteer.launch({
            executablePath:
            "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
            // executablePath:
            // "./node_modules/puppeteer/.local-chromium/win32-901912/chrome-win/chrome",
            headless:true,//無介面模式:有無需要開視窗,false要開,true不開
            slowMo:100,// slow down by 100ms
            devtools:false//有無需要開啟開發人員工具
        })
        page=await browser.newPage()
        //設定像素
        await page.setViewport({width:1200,height:1000})

        await page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
        await page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    })
    //after:每個test case執行之後統一要做的動作(page/browswer)
    after(async function(){
        await browser.close()
    })
    it('Go to Series Page - Check number',async function(){
        //step1:進去B2C各個Projector Series Page
        await page.goto('https://www.benq.com/en-us/projector/gaming-projector.html')
        const gamingProjector = " section.b2c-component-series-product-filter > div > div.right > ul.products > ul > li"
        //step2:計算每一個Series Page數量(ex:假設現在是10個, 之後數量如果增加了, 腳本就要改) 
        const countGamingProjector = await getCount(page, gamingProjector)
        console.log("Total of Gaming Projector:",countGamingProjector)
        expect(countGamingProjector).to.equal(8)//目前GamingProjector有8個產品
    })
    it('check each Spec is null or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/gaming-projector.html')
        //第一個projector
        var getGamingProjectorURL = " section.b2c-component-series-product-filter > div > div.right > ul.products > ul > li:nth-child(1) > a"
        var gamingProjectorProductURL = await page.$eval(getGamingProjectorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var gamingProjectorURL = gamingProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var gamingProjectorSpecURL = "https://www.benq.com/"+gamingProjectorURL
        console.log(gamingProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(gamingProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    it('check each Spec is null or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/gaming-projector.html')
        //第一個projector
        var getGamingProjectorURL = " section.b2c-component-series-product-filter > div > div.right > ul.products > ul > li:nth-child(2) > a"
        var gamingProjectorProductURL = await page.$eval(getGamingProjectorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var gamingProjectorURL = gamingProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var gamingProjectorSpecURL = "https://www.benq.com/"+gamingProjectorURL
        console.log(gamingProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(gamingProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    it('check each Spec is null or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/gaming-projector.html')
        //第一個projector
        var getGamingProjectorURL = " section.b2c-component-series-product-filter > div > div.right > ul.products > ul > li:nth-child(3) > a"
        var gamingProjectorProductURL = await page.$eval(getGamingProjectorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var gamingProjectorURL = gamingProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var gamingProjectorSpecURL = "https://www.benq.com/"+gamingProjectorURL
        console.log(gamingProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(gamingProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    it('check each Spec is null or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/gaming-projector.html')
        //第一個projector
        var getGamingProjectorURL = " section.b2c-component-series-product-filter > div > div.right > ul.products > ul > li:nth-child(4) > a"
        var gamingProjectorProductURL = await page.$eval(getGamingProjectorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var gamingProjectorURL = gamingProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var gamingProjectorSpecURL = "https://www.benq.com/"+gamingProjectorURL
        console.log(gamingProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(gamingProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    it('check each Spec is null or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/gaming-projector.html')
        //第一個projector
        var getGamingProjectorURL = " section.b2c-component-series-product-filter > div > div.right > ul.products > ul > li:nth-child(5) > a"
        var gamingProjectorProductURL = await page.$eval(getGamingProjectorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var gamingProjectorURL = gamingProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var gamingProjectorSpecURL = "https://www.benq.com/"+gamingProjectorURL
        console.log(gamingProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(gamingProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    it('check each Spec is null or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/gaming-projector.html')
        //第一個projector
        var getGamingProjectorURL = " section.b2c-component-series-product-filter > div > div.right > ul.products > ul > li:nth-child(6) > a"
        var gamingProjectorProductURL = await page.$eval(getGamingProjectorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var gamingProjectorURL = gamingProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var gamingProjectorSpecURL = "https://www.benq.com/"+gamingProjectorURL
        console.log(gamingProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(gamingProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    it('check each Spec is null or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/gaming-projector.html')
        //第一個projector
        var getGamingProjectorURL = " section.b2c-component-series-product-filter > div > div.right > ul.products > ul > li:nth-child(7) > a"
        var gamingProjectorProductURL = await page.$eval(getGamingProjectorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var gamingProjectorURL = gamingProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var gamingProjectorSpecURL = "https://www.benq.com/"+gamingProjectorURL
        console.log(gamingProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(gamingProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    it('check each Spec is null or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/gaming-projector.html')
        //第一個projector
        var getGamingProjectorURL = " section.b2c-component-series-product-filter > div > div.right > ul.products > ul > li:nth-child(8) > a"
        var gamingProjectorProductURL = await page.$eval(getGamingProjectorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var gamingProjectorURL = gamingProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var gamingProjectorSpecURL = "https://www.benq.com/"+gamingProjectorURL
        console.log(gamingProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(gamingProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
})
describe('B2C EN-US Projector - Home Projector - CinePro Series',()=>{
    let browser
    let page
    //Test Hooks:before, beforeEach, after, afterEach
    //before:每個test case執行之前先做的動作(page/browswer)
    before(async function(){
        browser=await puppeteer.launch({
            executablePath:
            "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
            // executablePath:
            // "./node_modules/puppeteer/.local-chromium/win32-901912/chrome-win/chrome",
            headless:true,//無介面模式:有無需要開視窗,false要開,true不開
            slowMo:100,// slow down by 100ms
            devtools:false//有無需要開啟開發人員工具
        })
        page=await browser.newPage()
        //設定像素
        await page.setViewport({width:1200,height:1000})

        await page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
        await page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    })
    //after:每個test case執行之後統一要做的動作(page/browswer)
    after(async function(){
        await browser.close()
    })
    it('Go to Series Page - Check number',async function(){
        await page.goto('https://www.benq.com/en-us/projector/cinepro-pro-cinema.html')
        const homeCineProProjector = "#seriesproducts_copy_ > div.right > ul.products > ul > li"
        //step2:計算每一個Series Page數量(ex:假設現在是10個, 之後數量如果增加了, 腳本就要改) 
        const countHomeCineProProjector = await getCount(page, homeCineProProjector)
        console.log("Total of HomeCinePro Projector:",countHomeCineProProjector)
        expect(countHomeCineProProjector).to.equal(2)//目前Home Projector - CinePro Series有2個產品
    })
    it('check each Spec is null or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/cinepro-pro-cinema.html')
        var getHomeCineProProjectorURL = "#seriesproducts_copy_ > div.right > ul.products > ul > li:nth-child(1) > a"
        var homeCineProProjectorProductURL = await page.$eval(getHomeCineProProjectorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var homeCineProProjectorURL = homeCineProProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var homeCineProProjectorSpecURL = "https://www.benq.com/"+homeCineProProjectorURL
        console.log(homeCineProProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(homeCineProProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    it('check each Spec is null or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/cinepro-pro-cinema.html')
        var getHomeCineProProjectorURL = "#seriesproducts_copy_ > div.right > ul.products > ul > li:nth-child(2) > a"
        var homeCineProProjectorProductURL = await page.$eval(getHomeCineProProjectorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var homeCineProProjectorURL = homeCineProProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var homeCineProProjectorSpecURL = "https://www.benq.com/"+homeCineProProjectorURL
        console.log(homeCineProProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(homeCineProProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
})
describe('B2C EN-US Projector - Home CinePrime Series',()=>{
    let browser
    let page
    //Test Hooks:before, beforeEach, after, afterEach
    //before:每個test case執行之前先做的動作(page/browswer)
    before(async function(){
        browser=await puppeteer.launch({
            executablePath:
            "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
            // executablePath:
            // "./node_modules/puppeteer/.local-chromium/win32-901912/chrome-win/chrome",
            headless:true,//無介面模式:有無需要開視窗,false要開,true不開
            slowMo:100,// slow down by 100ms
            devtools:false//有無需要開啟開發人員工具
        })
        page=await browser.newPage()
        //設定像素
        await page.setViewport({width:1200,height:1000})

        await page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
        await page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    })
    //after:每個test case執行之後統一要做的動作(page/browswer)
    after(async function(){
        await browser.close()
    })
    it('Go to Home CinePrime Series Page - Check number',async function(){
        //step1:進去B2C各個Projector Series Page
        await page.goto('https://www.benq.com/en-us/projector/cineprime-home-cinema.html')
        const homeCinePrimeProjector = "#seriesproducts_copy_ > div.right > ul.products > ul > li"
        //step2:計算每一個Series Page數量(ex:假設現在是10個, 之後數量如果增加了, 腳本就要改) 
        const countHomeCinePrimeProjector = await getCount(page, homeCinePrimeProjector)
        console.log("Total of Home Cine Prime Projector:",countHomeCinePrimeProjector)
        expect(countHomeCinePrimeProjector).to.equal(3)//目前homeCinePrimeProjector有3個產品
    })
    it('check each Spec is null or not',async function(){
        await page.goto('https://www.benq.com/en-us/projector/cineprime-home-cinema.html')
        var getHomeCinePrimeProjectorURL = "#seriesproducts_copy_ > div.right > ul.products > ul > li:nth-child(1) > a"
        var homeCinePrimeProjectorProductURL = await page.$eval(getHomeCinePrimeProjectorURL, element=> element.getAttribute("href"))
        // console.log(homeCinePrimeProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var homeCinePrimeProjectorURL = homeCinePrimeProjectorProductURL.replace(".html","/specifications.html")
        //console.log(homeCinePrimeProjectorURL)
        var homeCinePrimeProjectorSpecURL = "https://www.benq.com/"+homeCinePrimeProjectorURL
        console.log(homeCinePrimeProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(homeCinePrimeProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    it('check each Spec is null or not',async function(){
        await page.goto('https://www.benq.com/en-us/projector/cineprime-home-cinema.html')
        var getHomeCinePrimeProjectorURL = "#seriesproducts_copy_ > div.right > ul.products > ul > li:nth-child(2) > a"
        var homeCinePrimeProjectorProductURL = await page.$eval(getHomeCinePrimeProjectorURL, element=> element.getAttribute("href"))
        // console.log(homeCinePrimeProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var homeCinePrimeProjectorURL = homeCinePrimeProjectorProductURL.replace(".html","/specifications.html")
        //console.log(homeCinePrimeProjectorURL)
        var homeCinePrimeProjectorSpecURL = "https://www.benq.com/"+homeCinePrimeProjectorURL
        console.log(homeCinePrimeProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(homeCinePrimeProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    it('check each Spec is null or not',async function(){
        await page.goto('https://www.benq.com/en-us/projector/cineprime-home-cinema.html')
        var getHomeCinePrimeProjectorURL = "#seriesproducts_copy_ > div.right > ul.products > ul > li:nth-child(3) > a"
        var homeCinePrimeProjectorProductURL = await page.$eval(getHomeCinePrimeProjectorURL, element=> element.getAttribute("href"))
        // console.log(homeCinePrimeProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var homeCinePrimeProjectorURL = homeCinePrimeProjectorProductURL.replace(".html","/specifications.html")
        //console.log(homeCinePrimeProjectorURL)
        var homeCinePrimeProjectorSpecURL = "https://www.benq.com/"+homeCinePrimeProjectorURL
        console.log(homeCinePrimeProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(homeCinePrimeProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
})
describe('B2C EN-US Projector - Laser TV Projectors',()=>{
    let browser
    let page
    //Test Hooks:before, beforeEach, after, afterEach
    //before:每個test case執行之前先做的動作(page/browswer)
    before(async function(){
        browser=await puppeteer.launch({
            executablePath:
            "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
            // executablePath:
            // "./node_modules/puppeteer/.local-chromium/win32-901912/chrome-win/chrome",
            headless:true,//無介面模式:有無需要開視窗,false要開,true不開
            slowMo:100,// slow down by 100ms
            devtools:false//有無需要開啟開發人員工具
        })
        page=await browser.newPage()
        //設定像素
        await page.setViewport({width:1200,height:1000})

        await page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
        await page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    })
    //after:每個test case執行之後統一要做的動作(page/browswer)
    after(async function(){
        await browser.close()
    })
    it('Go to Laser TV Projectors Series Page - Check number',async function(){
        await page.goto('https://www.benq.com/en-us/projector/laser-tv-projector.html')
        const laserTVProjector = "#seriesproducts_copy_ > div.right > ul.products > ul > li"
        //step2:計算每一個Series Page數量(ex:假設現在是10個, 之後數量如果增加了, 腳本就要改) 
        const countLaserTVProjector = await getCount(page, laserTVProjector)
        console.log("Total of Laser TV Projector:",countLaserTVProjector)
        expect(countLaserTVProjector).to.equal(1)//目前Laser TV Projector有1個產品
    })
    it('check each Spec is null or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/laser-tv-projector.html')
        var getLaserTVProjectorURL = "#seriesproducts_copy_ > div.right > ul.products > ul > li > a"
        var laserTVProjectorProductURL = await page.$eval(getLaserTVProjectorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var laserTVProjectorURL = laserTVProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var laserTVProjectorSpecURL = "https://www.benq.com/"+laserTVProjectorURL
        console.log(laserTVProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(laserTVProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
})
describe('B2C EN-US Projector - Portable Projectors',()=>{
    let browser
    let page
    //Test Hooks:before, beforeEach, after, afterEach
    //before:每個test case執行之前先做的動作(page/browswer)
    before(async function(){
        browser=await puppeteer.launch({
            // executablePath:
            // "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
            executablePath:
            "./node_modules/puppeteer/.local-chromium/win32-901912/chrome-win/chrome",
            headless:true,//無介面模式:有無需要開視窗,false要開,true不開
            slowMo:100,// slow down by 100ms
            devtools:false//有無需要開啟開發人員工具
        })
        page=await browser.newPage()
        //設定像素
        await page.setViewport({width:1200,height:1000})

        await page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
        await page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    })
    //after:每個test case執行之後統一要做的動作(page/browswer)
    after(async function(){
        await browser.close()
    })
    it('Go to Series Page - Check number(GV Series)',async function(){
        //step1:進去B2C各個Projector Series Page
        await page.goto('https://www.benq.com/en-us/projector/portable.html')
        const gvPortableProjector = " #productlist_copy > div.right > ul.products > ul > li"
        //step2:計算每一個Series Page數量(ex:假設現在是10個, 之後數量如果增加了, 腳本就要改) 
        const countgvPortableProjector = await getCount(page, gvPortableProjector)
        console.log("Total of Portable Projector(GV Series):",countgvPortableProjector)
        expect(countgvPortableProjector).to.equal(2)//目前GamingProjector有8個產品
    })
    it('Go to Series Page - Check number(GS Series)',async function(){
        //step1:進去B2C各個Projector Series Page
        await page.goto('https://www.benq.com/en-us/projector/portable.html')
        const gsPortableProjector = " #productlist_copy_cop > div.right > ul.products > ul > li"
        //step2:計算每一個Series Page數量(ex:假設現在是10個, 之後數量如果增加了, 腳本就要改) 
        const countgsPortableProjector = await getCount(page, gsPortableProjector)
        console.log("Total of Portable Projector(GS Series):",countgsPortableProjector)
        expect(countgsPortableProjector).to.equal(2)//目前GamingProjector有8個產品
    })
    it('check all Spec exist or not(GV Series)',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/portable.html')
        //第一個projector
        var getgvPortableProjectorURL = " #productlist_copy > div.right > ul.products > ul > li:nth-child(1) > a"
        var gvPortableProjectorProductURL = await page.$eval(getgvPortableProjectorURL , element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var gvPortableProjectorURL  = gvPortableProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var gvPortableProjectorSpecURL = "https://www.benq.com/"+gvPortableProjectorURL
        console.log(gvPortableProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(gvPortableProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })

    it('check all Spec exist or not(GV Series)',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/portable.html')
        //第一個projector
        var getgvPortableProjectorURL = " #productlist_copy > div.right > ul.products > ul > li:nth-child(2) > a"
        var gvPortableProjectorProductURL = await page.$eval(getgvPortableProjectorURL , element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var gvPortableProjectorURL  = gvPortableProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var gvPortableProjectorSpecURL = "https://www.benq.com/"+gvPortableProjectorURL
        console.log(gvPortableProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(gvPortableProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })

    it('check all Spec exist or not(GS Series)',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/portable.html')
        //第一個projector
        var getgsPortableProjectorURL = " #productlist_copy_cop > div.right > ul.products > ul > li:nth-child(1) > a"
        var gsPortableProjectorProductURL = await page.$eval(getgsPortableProjectorURL , element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var gsPortableProjectorURL  = gsPortableProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var gsPortableProjectorSpecURL = "https://www.benq.com/"+gsPortableProjectorURL
        console.log(gsPortableProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(gsPortableProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })

    it('check all Spec exist or not(GS Series)',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/portable.html')
        //第一個projector
        var getgsPortableProjectorURL = " #productlist_copy_cop > div.right > ul.products > ul > li:nth-child(2) > a"
        var gsPortableProjectorProductURL = await page.$eval(getgsPortableProjectorURL , element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var gsPortableProjectorURL  = gsPortableProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var gsPortableProjectorSpecURL = "https://www.benq.com/"+gsPortableProjectorURL
        console.log(gsPortableProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(gsPortableProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
})

//BQtw, BQA, BQL, BQP
const titleName = '[B2C-ENUS]'
const ecUrlSelector = 'body > section.component-container-products.phase3-component-container-products > div > div > div:nth-child(2) > div > div.main_buy > a'
const ecUrl = await this.page.$eval(ecUrlSelector, element=> element.getAttribute("href"))
const ecUrlBuyExpect = "us-buy"
const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
const ecUrlCheck = ecUrl.indexOf(ecUrlBuyExpect)
if(ecUrlCheck <0 ){
    throw new Error(`${titleName} EC URL is not ${ecUrlBuyExpect}, it is ${ecUrlBuyActual} now. Here is the URL ${ecUrl}`)
}
//BQE
const titleName = '[B2C-ENEU]'
const ecUrlSelector = 'body > section.component-container-products.phase3-component-container-products > div > div > div:nth-child(2) > div > div.main_buy > a'
const ecUrl = await this.page.$eval(ecUrlSelector, element=> element.getAttribute("href"))
const ecUrlBuyExpect = "eu-buy"
const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
const ecUrlCheck = ecUrl.indexOf(ecUrlBuyExpect)
if(ecUrlCheck <0 ){
    throw new Error(`${titleName} EC URL is not ${ecUrlBuyExpect}, it is ${ecUrlBuyActual} now. Here is the URL ${ecUrl}`)
}
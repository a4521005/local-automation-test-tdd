//BQtw, BQA, BQL, BQP
const titleName = '[B2C-ENUS]'
const ecUrlSelector = 'body > section.component-container-products.phase3-component-container-products > div > div > div:nth-child(2) > div > div.main_buy > a'
const ecUrl = await this.page.$eval(ecUrlSelector, element=> element.getAttribute("href"))
const hostnameExpect = "buy.benq.com"
const hostnameActual = ecUrl.slice(8,20)//buy.benq.com
const ecUrlCheck = ecUrl.indexOf(hostnameExpect)
if(ecUrlCheck <0 ){
    throw new Error(`${titleName}  EC Host Name is not ${hostnameExpect}, it is ${hostnameActual} now. Here is the URL ${ecUrl}`)
}
//BQE
const titleName = '[B2C-ENEU]'
const ecUrlSelector = 'body > section.component-container-products.phase3-component-container-products > div > div > div:nth-child(2) > div > div.main_buy > a'
const ecUrl = await this.page.$eval(ecUrlSelector, element=> element.getAttribute("href"))
const hostnameExpect = "shop.benq.eu"
const hostnameActual = ecUrl.slice(8,20)//shop.benq.eu
const ecUrlCheck = ecUrl.indexOf(hostnameExpect)
if(ecUrlCheck <0 ){
    throw new Error(`${titleName}  EC Host Name is not ${hostnameExpect}, it is ${hostnameActual} now. Here is the URL ${ecUrl}`)
}

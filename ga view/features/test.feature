Feature:GA View Check
    tests:
    1. B2B URL
    2. Member Center(club-lang)
    3. EC Hostname and URL

    # Scenario: [B2C-ENUS] B2B URL
    #     Given Go to B2C-ENUS Page
    #     Then [B2C-ENUS] B2B URL must be en-us
    #     # Then [B2C-ENUS] It should have en-us_2B,2C_Master view
    #     # Then [B2C-ENUS] It should have en-us_B2B_Master view
    #     # Then [B2C-ENUS] It should have en-us_B2C_Master view
    # Scenario: [B2C-ENUS] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENUS Page
    #     When [B2C-ENUS] Go to Member Center
    #     Then [B2C-ENUS] club-lang must be en-us
    #     Then [B2C-ENUS] EC hostname must be buy.benq.com & EC URL must be us-buy

    # Scenario: [B2C-ENEU] B2B URL
    #     Given Go to B2C-ENEU Page
    #     Then [B2C-ENEU] B2B URL must be en-eu
    #     # Then [B2C-ENEU] It should have en-eu_2B,2C_Master view
    #     # Then [B2C-ENEU] It should have en-eu_B2B_Master view
    #     # Then [B2C-ENEU] It should have en-eu_B2C_Master view
    # Scenario: [B2C-ENEU] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENEU Page
    #     When [B2C-ENEU] Go to Member Center
    #     Then [B2C-ENEU] club-lang must be en-eu
    #     Then [B2C-ENEU] EC hostname must be shop.benq.eu && EC URL must be eu-buy

    # Scenario: [B2C-ENUK] B2B URL
    #     Given Go to B2C-ENUK Page
    #     Then [B2C-ENUK] B2B URL must be en-uk
    #     # Then [B2C-ENUK] It should have en-uk_2B,2C_Master view
    #     # Then [B2C-ENUK] It should have en-uk_B2B_Master view
    #     # Then [B2C-ENUK] It should have en-uk_B2C_Master view
    # Scenario: [B2C-ENUK] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENUK Page
    #     When [B2C-ENUK] Go to Member Center
    #     Then [B2C-ENUK] club-lang must be en-uk
    #     Then [B2C-ENUK] EC hostname must be shop.benq.eu & EC URL must be uk-buy

    # Scenario: [B2C-ENIE] B2B URL
    #     Given Go to B2C-ENIE Page
    #     Then [B2C-ENIE] B2B URL must be en-uk
    #     # Then [B2C-IE] It should have en-ie_B2C_Master view
    # Scenario: [B2C-ENIE] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENIE Page
    #     When [B2C-ENIE] Go to Member Center
    #     Then [B2C-ENIE] club-lang must be en-ie
    #     Then [B2C-ENIE] EC hostname must be shop.benq.eu & EC URL must be ie-buy
        
    # Scenario: [B2C-ENLU] B2B URL
    #     Given Go to B2C-ENLU Page
    #     Then [B2C-ENLU] B2B URL must be en-eu
    #     # Then [B2C-ENLU] It should have en-lu_B2C_Master view
    # Scenario: [B2C-ENLU] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENLU Page
    #     When [B2C-ENLU] Go to Member Center
    #     Then [B2C-ENLU] club-lang must be en-lu
    #     Then [B2C-ENLU] EC hostname must be shop.benq.eu & EC URL must be eu-buy
        
    # Scenario: [B2C-SVSE] B2B URL
    #     Given Go to B2C-SVSE Page
    #     Then [B2C-SVSE] B2B URL must be en-eu
    #     # Then [B2C-SVSE] It should have sv-se_B2C_Master view
    # Scenario: [B2C-SVSE] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-SVSE Page
    #     When [B2C-SVSE] Go to Member Center
    #     Then [B2C-SVSE] club-lang must be sv-se
    #     Then [B2C-SVSE] EC hostname must be shop.benq.eu & EC URL must be sc-buy

    # Scenario: [B2C-ENCEE] B2B URL
    #     Given Go to B2C-ENCEE Page
    #     Then [B2C-ENCEE] B2B URL must be en-eu
    #     # Then [B2C-ENCEE] It should have en-cee_B2C_Master view
    # Scenario: [B2C-ENCEE] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENCEE Page
    #     Then [B2C-ENCEE] B2C-ENCEE Page does not have Member Center
    #     Then [B2C-ENCEE] B2C-ENCEE Page does not have EC Hostname and URL
   
    # Scenario: [B2C-FRFR] B2B URL
    #     Given Go to B2C-FRFR Page
    #     Then [B2C-FRFR] B2B URL must be fr-fr
    #     # Then [B2C-FRFR] It should have fr-fr_2B,2C_Master view
    #     # Then [B2C-FRFR] It should have fr-fr_B2B_Master view
    #     # Then [B2C-FRFR] It should have fr-fr_B2C_Master view
    # Scenario: [B2C-FRFR] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-FRFR Page
    #     When [B2C-FRFR] Go to Member Center
    #     Then [B2C-FRFR] club-lang must be fr-fr
    #     Then [B2C-FRFR] EC hostname must be shop.benq.eu & EC URL must be fr-buy

    # Scenario: [B2C-FRCH] B2B URL
    #     Given Go to B2C-FRCH Page
    #     Then [B2C-FRCH] B2B URL must be fr-fr
    #     # Then [B2C-FRCH] It should have fr-ch_B2C_Master view
    # Scenario: [B2C-FRCH] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-FRCH Page
    #     When [B2C-FRCH] Go to Member Center
    #     Then [B2C-FRCH] club-lang must be fr-ch
    #     Then [B2C-FRCH] B2C-FRCH Page does not have EC Hostname and URL

    # Scenario: [B2C-ESES] B2B URL
    #     Given Go to B2C-ESES Page
    #     Then [B2C-ESES] B2B URL must be es-es
    #     # Then [B2C-ESES] It should have es-es_2B,2C_Master view
    #     # Then [B2C-ESES] It should have es-es_B2B_Master view
    #     # Then [B2C-ESES] It should have es-es_B2C_Master view
    # Scenario: [B2C-ESES] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ESES Page
    #     When [B2C-ESES] Go to Member Center
    #     Then [B2C-ESES] club-lang must be es-es
    #     Then [B2C-ESES] EC hostname must be shop.benq.eu & EC URL must be es-buy

    # Scenario: [B2C-ITIT] B2B URL
    #     Given Go to B2C-ITIT Page
    #     Then [B2C-ITIT] B2B URL must be it-it
    #     # Then [B2C-ITIT] It should have it-it_2B,2C_Master view
    #     # Then [B2C-ITIT] It should have it-it_B2B_Master view
    #     # Then [B2C-ITIT] It should have it-it_B2C_Master view
    # Scenario: [B2C-ITIT] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ITIT Page
    #     When [B2C-ITIT] Go to Member Center
    #     Then [B2C-ITIT] club-lang must be it-it
    #     Then [B2C-ITIT] EC hostname must be shop.benq.eu & EC URL must be it-buy

    # Scenario: [B2C-PTPT] B2B URL
    #     Given Go to B2C-PTPT Page
    #     Then [B2C-PTPT] B2B URL must be en-eu
    #     # Then [B2C-PTPT] It should have pt-pt_B2C_Master view
    # Scenario: [B2C-PTPT] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-PTPT Page
    #     When [B2C-PTPT] Go to Member Center
    #     Then [B2C-PTPT] club-lang must be pt-pt
    #     Then [B2C-PTPT] B2C-PTPT Page does not have EC Hostname and URL

    # Scenario: [B2C-DEDE] B2B URL
    #     Given Go to B2C-DEDE Page
    #     Then [B2C-DEDE] B2B URL must be de-de
    #     # Then [B2C-DEDE] It should have de-de_2B,2C_Master view
    #     # Then [B2C-DEDE] It should have de-de_B2B_Master view
    #     # Then [B2C-DEDE] It should have de-de_B2C_Master view
    # Scenario: [B2C-DEDE] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-DEDE Page
    #     When [B2C-DEDE] Go to Member Center
    #     Then [B2C-DEDE] club-lang must be de-de
    #     Then [B2C-DEDE] EC hostname must be shop.benq.eu & EC URL must be de-buy

    # Scenario: [B2C-DEAT] B2B URL
    #     Given Go to B2C-DEAT Page
    #     Then [B2C-DEAT] B2B URL must be de-de
    #      # Then [B2C-DEAT] It should have de-at_B2C_Master view
    # Scenario: [B2C-DEAT] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-DEAT Page
    #     When [B2C-DEAT] Go to Member Center
    #     Then [B2C-DEAT] club-lang must be de-de
    #     Then [B2C-DEAT] B2C-DEAT Page does not have EC Hostname and URL

    # Scenario: [B2C-DECH] B2B URL
    #     Given Go to B2C-DECH Page
    #     Then [B2C-DECH] B2B URL must be de-de
    #     # Then [B2C-DECH] It should have en-cee_B2C_Master view
    # Scenario: [B2C-DECH] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-DECH Page
    #     Then [B2C-DECH] B2C-DECH Page does not have Member Center
    #     Then [B2C-DECH] B2C-DECH Page does not have EC Hostname and URL

    # Scenario: [B2C-NLNL] B2B URL
    #     Given Go to B2C-NLNL Page
    #     Then [B2C-NLNL] B2B URL must be nl-nl
    #     # Then [B2C-NLNL] It should have nl-nl_2B,2C_Master view
    #     # Then [B2C-NLNL] It should have nl-nl_B2B_Master view
    #     # Then [B2C-NLNL] It should have nl-nl_B2C_Master view
    # Scenario: [B2C-NLNL] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-NLNL Page
    #     When [B2C-NLNL] Go to Member Center
    #     Then [B2C-NLNL] club-lang must be nl-nl
    #     Then [B2C-NLNL] EC hostname must be shop.benq.eu & EC URL must be nl-buy

    # Scenario: [B2C-NLBE] B2B URL
    #     Given Go to B2C-NLBE Page
    #     Then [B2C-NLBE] B2B URL must be nl-nl
    #      # Then [B2C-NLBE] It should have nl-nl_B2C_Master view
    # Scenario: [B2C-NLBE] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-NLBE Page
    #     When [B2C-NLBE] Go to Member Center
    #     Then [B2C-NLBE] club-lang must be nl-nl
    #     Then [B2C-NLBE] B2C-NLBE Page does not have EC Hostname and URL

    # Scenario: [B2C-ENNO] B2B URL
    #     Given Go to B2C-ENNO Page
    #     Then [B2C-ENNO] B2B URL must be en-eu
    #     # Then [B2C-ENNO] It should have en-cee_B2C_Master view
    # Scenario: [B2C-ENNO] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENNO Page
    #     Then [B2C-ENNO] B2C-ENNO Page does not have Member Center
    #     Then [B2C-ENNO] B2C-ENNO Page does not have EC Hostname and URL

    # Scenario: [B2C-ENFI] B2B URL
    #     Given Go to B2C-ENFI Page
    #     Then [B2C-ENFI] B2B URL must be en-eu
    #     # Then [B2C-ENFI] It should have en-fi_B2C_Master view
    # Scenario: [B2C-ENFI] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENFI Page
    #     When [B2C-ENFI] Go to Member Center
    #     Then [B2C-ENFI] club-lang must be en-fi
    #     Then [B2C-ENFI] EC hostname must be shop.benq.eu && EC URL must be sc-buy

    # Scenario: [B2C-ENDK] B2B URL
    #     Given Go to B2C-ENDK Page
    #     Then [B2C-ENDK] B2B URL must be en-eu
    #     # Then [B2C-ENDK] It should have en-dk_B2C_Master view
    # Scenario: [B2C-ENDK] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENDK Page
    #     When [B2C-ENDK] Go to Member Center
    #     Then [B2C-ENDK] club-lang must be en-dk
    #     Then [B2C-ENDK] EC hostname must be shop.benq.eu && EC URL must be sc-buy

    # Scenario: [B2C-ENIS] B2B URL
    #     Given Go to B2C-ENIS Page
    #     Then [B2C-ENIS] B2B URL must be en-eu
    #     # Then [B2C-ENIS] It should have en-cee_B2C_Master view
    # Scenario: [B2C-ENIS] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENIS Page
    #     Then [B2C-ENIS] B2C-ENIS Page does not have Member Center
    #     Then [B2C-ENIS] B2C-ENIS Page does not have EC Hostname and URL

    # Scenario: [B2C-RURU] B2B URL
    #     Given Go to B2C-RURU Page
    #     Then [B2C-RURU] B2B URL must be ru-ru
    #     # Then [B2C-RURU] It should have ru-ru_2B,2C_Master view
    #     # Then [B2C-RURU] It should have ru-ru_B2B_Master view
    #     # Then [B2C-RURU] It should have ru-ru_B2C_Master view
    # Scenario: [B2C-RURU] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-RURU Page
    #     Then [B2C-RURU] B2C-RURU Page does not have Member Center
    #     Then [B2C-RURU] B2C-RURU Page does not have EC Hostname and URL

    # Scenario: [B2C-PLPL] B2B URL
    #     Given Go to B2C-PLPL Page
    #     Then [B2C-PLPL] B2B URL must be pl-pl
    #     # Then [B2C-PLPL] It should have pl-pl_2B,2C_Master view
    #     # Then [B2C-PLPL] It should have pl-pl_B2B_Master view
    #     # Then [B2C-PLPL] It should have pl-pl_B2C_Master view
    # Scenario: [B2C-PLPL] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-PLPL Page
    #     When [B2C-PLPL] Go to Member Center
    #     Then [B2C-PLPL] club-lang must be en-eu
    #     Then [B2C-PLPL] EC hostname must be shop.benq.eu && EC URL must be eu-buy

    # Scenario: [B2C-BGBG] B2B URL
    #     Given Go to B2C-BGBG Page
    #     Then [B2C-BGBG] B2B URL must be en-eu
    #     # Then [B2C-BGBG] It should have bg-bg_B2C_Master view
    # Scenario: [B2C-BGBG] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-BGBG Page
    #     Then [B2C-BGBG] B2C-BGBG Page does not have Member Center
    #     Then [B2C-BGBG] B2C-BGBG Page does not have EC Hostname and URL

    # Scenario: [B2C-CSCZ] B2B URL
    #     Given Go to B2C-CSCZ Page
    #     Then [B2C-CSCZ] B2B URL must be cs-cz
    #     # Then [B2C-CSCZ] It should have cs-cz_2B,2C_Master view
    #     # Then [B2C-CSCZ] It should have cs-cz_B2B_Master view
    #     # Then [B2C-CSCZ] It should have cs-cz_B2C_Master view
    # Scenario: [B2C-CSCZ] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-CSCZ Page
    #     When [B2C-CSCZ] Go to Member Center
    #     Then [B2C-CSCZ] club-lang must be en-eu
    #     Then [B2C-CSCZ] EC hostname must be shop.benq.eu && EC URL must be eu-buy

    # Scenario: [B2C-ELGR] B2B URL
    #     Given Go to B2C-ELGR Page
    #     Then [B2C-ELGR] B2B URL must be en-eu
    #     # Then [B2C-ELGR] It should have el-gr_B2C_Master view
    # Scenario: [B2C-ELGR] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ELGR Page
    #     Then [B2C-ELGR] B2C-ELGR Page does not have Member Center
    #     Then [B2C-ELGR] B2C-ELGR Page does not have EC Hostname and URL

    # Scenario: [B2C-HUHU] B2B URL
    #     Given Go to B2C-HUHU Page
    #     Then [B2C-HUHU] B2B URL must be en-eu
    #     # Then [B2C-HUHU] It should have hu-hu_B2C_Master view
    # Scenario: [B2C-HUHU] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-HUHU Page
    #     Then [B2C-HUHU] B2C-HUHU Page does not have Member Center
    #     Then [B2C-HUHU] B2C-HUHU Page does not have EC Hostname and URL

    # Scenario: [B2C-LTLT] B2B URL
    #     Given Go to B2C-LTLT Page
    #     Then [B2C-LTLT] B2B URL must be en-eu
    #     # Then [B2C-LTLT] It should have lt-lt_B2C_Master view
    # Scenario: [B2C-LTLT] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-LTLT Page
    #     Then [B2C-LTLT] B2C-LTLT Page does not have Member Center
    #     Then [B2C-LTLT] B2C-LTLT Page does not have EC Hostname and URL
    
    # Scenario: [B2C-RORO] B2B URL
    #     Given Go to B2C-RORO Page
    #     Then [B2C-RORO] B2B URL must be en-eu
    #     # Then [B2C-RORO] It should have ro-ro_B2C_Master view
    # Scenario: [B2C-RORO] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-RORO Page
    #     Then [B2C-RORO] B2C-RORO Page does not have Member Center
    #     Then [B2C-RORO] B2C-RORO Page does not have EC Hostname and URL
    
    # Scenario: [B2C-SKSK] B2B URL
    #     Given Go to B2C-SKSK Page
    #     Then [B2C-SKSK] B2B URL must be en-eu
    #     # Then [B2C-SKSK] It should have sk-sk_B2C_Master view
    # Scenario: [B2C-SKSK] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-SKSK Page
    #     Then [B2C-SKSK] B2C-SKSK Page does not have Member Center
    #     Then [B2C-SKSK] B2C-SKSK Page does not have EC Hostname and URL

    Scenario: [B2C-UKUA] B2B URL
        Given Go to B2C-UKUA Page
        Then [B2C-UKUA] B2B URL must be en-eu
        # Then [B2C-UKUA] It should have uk-ua_B2C_Master view
    Scenario: [B2C-UKUA] Member Center(club-lang) & EC Hostname and URL
        Given Go to B2C-UKUA Page
        Then [B2C-UKUA] B2C-UKUA Page does not have Member Center
        Then [B2C-UKUA] B2C-UKUA Page does not have EC Hostname and URL

    Scenario: [B2C-ENLV] B2B URL
        Given Go to B2C-ENLV Page
        Then [B2C-ENLV] B2B URL must be en-eu
        # Then [B2C-ENLV] It should have en-lv_B2C_Master view
    Scenario: [B2C-ENLV] Member Center(club-lang) & EC Hostname and URL
        Given Go to B2C-ENLV Page
        Then [B2C-ENLV] B2C-ENLV Page does not have Member Center
        Then [B2C-ENLV] B2C-ENLV Page does not have EC Hostname and URL

    Scenario: [B2C-ENRS] B2B URL
        Given Go to B2C-ENRS Page
        Then [B2C-ENRS] B2B URL must be en-eu
        # Then [B2C-ENRS] It should have en-rs_B2C_Master view
    Scenario: [B2C-ENRS] Member Center(club-lang) & EC Hostname and URL
        Given Go to B2C-ENRS Page
        Then [B2C-ENRS] B2C-ENRS Page does not have Member Center
        Then [B2C-ENRS] B2C-ENRS Page does not have EC Hostname and URL

    Scenario: [B2C-ENSI] B2B URL
        Given Go to B2C-ENSI Page
        Then [B2C-ENSI] B2B URL must be en-eu
        # Then [B2C-ENSI] It should have en-si_B2C_Master view
    Scenario: [B2C-ENSI] Member Center(club-lang) & EC Hostname and URL
        Given Go to B2C-ENSI Page
        Then [B2C-ENSI] B2C-ENSI Page does not have Member Center
        Then [B2C-ENSI] B2C-ENSI Page does not have EC Hostname and URL

    Scenario: [B2C-ENBA] B2B URL
        Given Go to B2C-ENBA Page
        Then [B2C-ENBA] B2B URL must be en-eu
        # Then [B2C-ENBA] It should have en-ba_B2C_Master view
    Scenario: [B2C-ENBA] Member Center(club-lang) & EC Hostname and URL
        Given Go to B2C-ENBA Page
        Then [B2C-ENBA] B2C-ENBA Page does not have Member Center
        Then [B2C-ENBA] B2C-ENBA Page does not have EC Hostname and URL

    Scenario: [B2C-ENCY] B2B URL
        Given Go to B2C-ENCY Page
        Then [B2C-ENCY] B2B URL must be en-eu
        # Then [B2C-ENCY] It should have en-cy_B2C_Master view
    Scenario: [B2C-ENCY] Member Center(club-lang) & EC Hostname and URL
        Given Go to B2C-ENCY Page
        Then [B2C-ENCY] B2C-ENCY Page does not have Member Center
        Then [B2C-ENCY] B2C-ENCY Page does not have EC Hostname and URL

    Scenario: [B2C-ENEE] B2B URL
        Given Go to B2C-ENEE Page
        Then [B2C-ENEE] B2B URL must be en-eu
        # Then [B2C-ENEE] It should have en-ee_B2C_Master view
    Scenario: [B2C-ENEE] Member Center(club-lang) & EC Hostname and URL
        Given Go to B2C-ENEE Page
        Then [B2C-ENEE] B2C-ENEE Page does not have Member Center
        Then [B2C-ENEE] B2C-ENEE Page does not have EC Hostname and URL

    Scenario: [B2C-ENHR] B2B URL
        Given Go to B2C-ENHR Page
        Then [B2C-ENHR] B2B URL must be en-eu
        # Then [B2C-ENHR] It should have en-hr_B2C_Master view
    Scenario: [B2C-ENHR] Member Center(club-lang) & EC Hostname and URL
        Given Go to B2C-ENHR Page
        Then [B2C-ENHR] B2C-ENHR Page does not have Member Center
        Then [B2C-ENHR] B2C-ENHR Page does not have EC Hostname and URL

    Scenario: [B2C-ENMK] B2B URL
        Given Go to B2C-ENMK Page
        Then [B2C-ENMK] B2B URL must be en-eu
        # Then [B2C-ENMK] It should have en-mk_B2C_Master view
    Scenario: [B2C-ENMK] Member Center(club-lang) & EC Hostname and URL
        Given Go to B2C-ENMK Page
        Then [B2C-ENMK] B2C-ENMK Page does not have Member Center
        Then [B2C-ENMK] B2C-ENMK Page does not have EC Hostname and URL

    Scenario: [B2C-ENMT] B2B URL
        Given Go to B2C-ENMT Page
        Then [B2C-ENMT] B2B URL must be en-eu
        # Then [B2C-ENMT] It should have en-mt_B2C_Master view
    Scenario: [B2C-ENMT] Member Center(club-lang) & EC Hostname and URL
        Given Go to B2C-ENMT Page
        Then [B2C-ENMT] B2C-ENMT Page does not have Member Center
        Then [B2C-ENMT] B2C-ENMT Page does not have EC Hostname and URL

    # Scenario: [B2C-ENAP] B2B URL
    #     Given Go to B2C-ENAP Page
    #     Then [B2C-ENAP] B2B URL must be en-ap
    #     # Then [B2C-ENAP] It should have en-ap_2B,2C_Master view
    #     # Then [B2C-ENAP] It should have en-ap_B2B_Master view
    #     # Then [B2C-ENAP] It should have en-ap_B2C_Master view
    # Scenario: [B2C-ENAP] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENAP Page
    #     Then [B2C-ENAP] B2C-ENAP Page does not have Member Center
    #     Then [B2C-ENAP] B2C-ENAP Page does not have EC Hostname and URL
    
    # Scenario: [B2C-FRCA] B2B URL
    #     Given Go to B2C-FRCA Page
    #     Then [B2C-FRCA] B2B URL must be fr-ca
    #     # Then [B2C-FRCA] It should have fr-ca_2B,2C_Master view
    #     # Then [B2C-FRCA] It should have fr-ca_B2B_Master view
    #     # Then [B2C-FRCA] It should have fr-ca|fr-ca_B2C_Master view
    # Scenario: [B2C-FRCA] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-FRCA Page
    #     Then [B2C-FRCA] B2C-FRCA Page does not have Member Center
    #     Then [B2C-FRCA] B2C-FRCA Page does not have EC Hostname and URL







    
        

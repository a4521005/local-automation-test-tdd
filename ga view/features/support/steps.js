const { Given, When, Then, Before, After } = require('@cucumber/cucumber')
const cheerio = require("cheerio");
const { expect } = require('chai')
const fs = require('fs');
const puppeteer = require('puppeteer')
const request = require("request-promise");
const cicGA="?utm_source=autotest&utm_medium=CIC"

Before({timeout: 24 * 5000},async function () {
    this.browser = await puppeteer.launch({ 
        executablePath:
        "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
        headless:false,//有無需要開視窗,false要開,true不開
        slowMo:100,// slow down by 100ms
        devtools:false//有無需要開啟開發人員工具
    })
    this.page = await this.browser.newPage()
    await this.page.setViewport({width:1200,height:1000})
    await this.page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
    await this.page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
})

After({timeout: 12 * 5000},async function () {
    await this.browser.close()
})

Given("Go to B2C-ENUS Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.com/en-us/index.html'+cicGA)
})

Then("[B2C-ENUS] B2B URL must be en-us",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENUS]'
    const b2bName = 'en-us'
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.com/"," ")
    const b2bURLRemoveHostNameAndIndexHtml = b2bURLRemoveHostName.replace("/business/index.html"," ")
    const b2bURLCheck = b2bURLRemoveHostNameAndIndexHtml.indexOf('en-us')
    //目前en-us的B2B URL是en-us
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bName} in the past, but it is ${b2bURLRemoveHostNameAndIndexHtml} now. Here is the URL ${b2bURL}`)
    }
})

When("[B2C-ENUS] Go to Member Center",{timeout: 12 * 5000},async function(){
    const titleName = '[B2C-ENUS]'
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    //以前有member center但現在沒有的話
    //console.log('memberCenterCheck:',memberCenterCheck)
    if(memberCenterCheck <0){
        throw new Error(`${titleName} had member center in the past, but it does not have member center now.`)
    }
    
})

Then("[B2C-ENUS] club-lang must be en-us",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENUS]'
    const signInSelector = 'body > header > aside.line-1 > aside > nav > div.right-wrapper > div.log_area > a'
    await this.page.waitForSelector(signInSelector)
    await this.page.click(signInSelector)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    await this.page.waitForSelector('#AccountLogin')
    const memberCenterClublangurl = await this.page.url()
    //console.log("EN-US club-lang URL:",memberCenterClublangurl)
    const clubExpect = "club.benq.com"
    const clubActual = memberCenterClublangurl.slice(8,21)
    const clubLangExpect = "lang=en-us"
    const clubLangActual = memberCenterClublangurl.slice(69,79)
    //const clubLangRemoveLang = clubLangExpect.replace("lang="," ")
    const clubActualCheck = memberCenterClublangurl.indexOf(clubExpect)
    const clubLangActualCheck = memberCenterClublangurl.indexOf(clubLangExpect)
    if(clubActualCheck <0 && clubLangActualCheck <0 ){
        throw new Error(`${titleName} member center URL does not include ${clubExpect} and club-lang is not ${clubLangExpect} , member center URL includes ${clubActual} and club-lang is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubActualCheck<0){
        throw new Error(`${titleName} member center URL does not include ${clubExpect}, it is ${clubActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubLangActualCheck <0 ){
        throw new Error(`${titleName} club-lang is not ${clubLangExpect}, it is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    // expect(b2bClublangurl).to.include('club.benq.com')
    // expect(b2bClublangurl).to.include('lang=en-us')
})

Given("Go to B2C-ENUS Product Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.com/en-us/lamps/desklamp/desklamp-genie.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
})

Then("[B2C-ENUS] EC hostname must be buy.benq.com & EC URL must be us-buy",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENUS]'
    const hostnameExpect = "buy.benq.com"
    const productInfoUrl = "https://www.benq.com/en-us/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
    //console.log(productInfoJson)
    //如果以前有EC但現在沒了EC
    const noEC = []
    for(let i =0; i<productInfoJson.length ; i++){
        const noECCheck = productInfoJson[i].checkProduct
        const noEcUrlBuy = ""//以前如果沒EC的話就是空的
        if(noECCheck ==noEcUrlBuy || noECCheck.indexOf("us-buy")<0){
            noEC.push(noECCheck)
        }
    }
    //console.log("noEC:",noEC)
    if(noEC.length === productInfoJson.length){
        throw new Error(`${titleName}  had EC in the past, but now it did not have EC now.`)
    }
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].checkProduct
        const ecHostName = productInfoJson[i].buyNowlink
        if(ecUrl !== "" && ecHostName.indexOf("-buy")>0 && ecHostName.indexOf(hostnameExpect)<0){
            const hostNameActual = ecHostName.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  EC Host Name is not ${hostnameExpect}, it is ${hostNameActual} now. Here is the URL ${ecHostName}`)
        }
    }
    const ecUrlBuyExpect = "us-buy"
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].checkProduct
        //console.log(ecUrl)
        if(ecUrl !== "" && ecUrl.indexOf("-buy")>0 && ecUrl.indexOf(ecUrlBuyExpect)<0){
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            throw new Error(`${titleName}  EC Host Name is not ${ecUrlBuyExpect}, it is ${ecUrlBuyActual} now. Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-ENAP Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.com/en-ap/index.html'+cicGA)
})

Then("[B2C-ENAP] B2B URL must be en-ap",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENAP]'
    const b2bNameExpect = 'en-ap'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.com/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

Then("[B2C-ENAP] B2C-ENAP Page does not have Member Center",{timeout: 124 * 5000},async function(){
    const titleName = '[B2C-ENAP]'
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    //目前en-ap沒有member center
    //以前沒有member center但現在有的話
    if(memberCenterCheck > 0){
        throw new Error(`${titleName}  did not have member center in the past, but it have member center now.`)
    }
})

Then("[B2C-ENAP] B2C-ENAP Page does not have EC Hostname and URL",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENAP]'
    const productInfoUrl = "https://www.benq.com/en-ap/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
        //console.log(productInfoJson)
    const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].buyNowlink
        const ecCheck = productInfoJson[i].checkProduct
        //console.log(ecUrl)
        if(ecCheck !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
            //因為ENAP有些buyNowlink是us-buy, 官網上沒開EC, 但是Product Json上有
            //https://www.benq.com/en-ap/projector/accessory/carrybag-ql01.html
            //buyNowlink: "https://buy.benq.com/us-buy/soft-carrying-case-for-ht3550-tk850-tk810-tk800m-ht2150st-mh750-mh760-ht2050-ht3050-ht2550-tk800.html"
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-FRCA Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.com/fr-ca/index.html'+cicGA)
})

Then("[B2C-FRCA] B2B URL must be fr-ca",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-FRCA]'
    const b2bNameExpect = 'fr-ca'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.com/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

Then("[B2C-FRCA] B2C-FRCA Page does not have Member Center",{timeout: 124 * 5000},async function(){
    const titleName = '[B2C-FRCA]'
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    //目前en-ap沒有member center
    //以前沒有member center但現在有的話
    if(memberCenterCheck > 0){
        throw new Error(`${titleName}  did not have member center in the past, but it have member center now.`)
    }
})

Then("[B2C-FRCA] B2C-FRCA Page does not have EC Hostname and URL",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-FRCA]'
    const productInfoUrl = "https://www.benq.com/fr-ca/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
    //console.log(productInfoJson)
    const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].buyNowlink
        const ecCheck = productInfoJson[i].checkProduct
        //console.log(ecUrl)
        if(ecCheck !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-ENEU Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/en-eu/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-ENEU] B2B URL must be en-eu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENEU]'
    const b2bNameExpect = 'en-eu'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

When("[B2C-ENEU] Go to Member Center",{timeout: 12 * 5000},async function(){
    const titleName = '[B2C-ENEU]'
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    if(memberCenterCheck <0){
        throw new Error(`${titleName} had member center in the past, but it does not have member center now.`)
    }
})

Then("[B2C-ENEU] club-lang must be en-eu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENEU]'
    const signInSelector = 'body > header > aside.line-1 > aside > nav > div.right-wrapper > div.log_area > a'
    await this.page.waitForSelector(signInSelector)
    await this.page.click(signInSelector)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    await this.page.waitForSelector('#AccountLogin')
    const memberCenterClublangurl = await this.page.url()
    //https://club.benq.eu/ICDS_EU/Home/BenQAuth?system_id=G5&function=Login&lang=en-eu&return_url=https%3A%2F%2Fwww.benq.eu%2Fen-eu%2Findex.login.html
    const clubExpect = "club.benq.eu"
    const clubActual = memberCenterClublangurl.slice(8,20)
    const clubLangExpect = "lang=en-eu"
    const clubLangActual = memberCenterClublangurl.slice(71,81)
    const clubActualCheck = memberCenterClublangurl.indexOf(clubExpect)
    const clubLangActualCheck = memberCenterClublangurl.indexOf(clubLangExpect)
    if(clubActualCheck <0 && clubLangActualCheck <0 ){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect} and club-lang is not ${clubLangExpect} , member center URL includes ${clubActual} and club-lang is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubActualCheck<0){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect}, it is ${clubActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubLangActualCheck <0 ){
        throw new Error(`${titleName} club-lang is not ${clubLangExpect}, it is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
})

Then("[B2C-ENEU] EC hostname must be shop.benq.eu && EC URL must be eu-buy",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENEU]'
    const hostnameExpect = "shop.benq.eu"
    const productInfoUrl = "https://www.benq.eu/en-eu/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
    //console.log(productInfoJson)
    //如果以前有EC但現在沒了EC
    const noEC = []
    for(let i =0; i<productInfoJson.length ; i++){
        const noECCheck = productInfoJson[i].checkProduct
        const noEcUrlBuy = ""//以前如果沒EC的話就是空的
        if(noECCheck ==noEcUrlBuy || noECCheck.indexOf("eu-buy")<0){
            noEC.push(noECCheck)
        }
    }
    //console.log("noEC:",noEC)
    if(noEC.length === productInfoJson.length){
        throw new Error(`${titleName}  had EC in the past, but now it did not have EC now.`)
    }
    for(let i =0; i<productInfoJson.length ; i++){
        const ecCheck = productInfoJson[i].checkProduct
        const ecHostName = productInfoJson[i].buyNowlink
        if(ecCheck !== "" && ecHostName.indexOf("-buy")>0 && ecHostName.indexOf(hostnameExpect)<0){
            const hostNameActual = ecHostName.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  EC Host Name is not ${hostnameExpect}, it is ${hostNameActual} now. Here is the URL ${ecHostName}`)
        }
    }
    const ecUrlBuyExpect = "eu-buy"
    for(let i =0; i<productInfoJson.length ; i++){
        const ecCheck = productInfoJson[i].checkProduct
        const ecUrl= productInfoJson[i].buyNowlink
        //console.log(ecUrl)
        if(ecCheck !== "" && ecCheck.indexOf("-buy")>0 && ecCheck.indexOf(ecUrlBuyExpect)<0){
            const ecUrlBuyActual = ecCheck.slice(21,27) //xx-buy
            throw new Error(`${titleName}  EC Host Name is not ${ecUrlBuyExpect}, it is ${ecUrlBuyActual} now. Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-DEAT Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/de-at/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-DEAT] B2B URL must be de-de",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-DEAT]'
    const b2bNameExpect = 'de-de'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

When("[B2C-DEAT] Go to Member Center",{timeout: 12 * 5000},async function(){
    const titleName = '[B2C-DEAT]'
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    if(memberCenterCheck <0){
        throw new Error(`${titleName} had member center in the past, but it does not have member center now.`)
    }
})

Then("[B2C-DEAT] club-lang must be de-de",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-DEAT]'
    const signInSelector = 'body > header > aside.line-1 > aside > nav > div.right-wrapper > div.log_area > a'
    await this.page.waitForSelector(signInSelector)
    await this.page.click(signInSelector)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    await this.page.waitForSelector('#AccountLogin')
    const memberCenterClublangurl = await this.page.url()
    //https://club.benq.eu/ICDS_EU/Home/BenQAuth?system_id=G5&function=Login&lang=de-at&return_url=https%3A%2F%2Fwww.benq.eu%2Fde-at%2Findex.login.html
    const clubExpect = "club.benq.eu"
    const clubActual = memberCenterClublangurl.slice(8,20)
    const clubLangExpect = "lang=de-at"
    const clubLangActual = memberCenterClublangurl.slice(71,81)
    const clubActualCheck = memberCenterClublangurl.indexOf(clubExpect)
    const clubLangActualCheck = memberCenterClublangurl.indexOf(clubLangExpect)
    if(clubActualCheck <0 && clubLangActualCheck <0 ){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect} and club-lang is not ${clubLangExpect} , member center URL includes ${clubActual} and club-lang is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubActualCheck<0){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect}, it is ${clubActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubLangActualCheck <0 ){
        throw new Error(`${titleName} club-lang is not ${clubLangExpect}, it is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
})

Then("[B2C-DEAT] B2C-DEAT Page does not have EC Hostname and URL",{timeout: 120 * 5000},async function(){
    const titleName = '[B2C-DEAT]'
    const productInfoUrl = "https://www.benq.eu/de-at/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
    //console.log(productInfoJson)
    const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].buyNowlink
        const ecCheck = productInfoJson[i].checkProduct
        //console.log(ecUrl)
        if(ecCheck !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
            //因為ENAP有些buyNowlink是us-buy, 官網上沒開EC, 但是Product Json上有
            //https://www.benq.com/en-ap/projector/accessory/carrybag-ql01.html
            //buyNowlink: "https://buy.benq.com/us-buy/soft-carrying-case-for-ht3550-tk850-tk810-tk800m-ht2150st-mh750-mh760-ht2050-ht3050-ht2550-tk800.html"
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-CSCZ Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/cs-cz/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-CSCZ] B2B URL must be cs-cz",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-CSCZ]'
    const b2bNameExpect = 'cs-cz'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

When("[B2C-CSCZ] Go to Member Center",{timeout: 12 * 5000},async function(){
    const titleName = '[B2C-CSCZ]'
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    if(memberCenterCheck <0){
        throw new Error(`${titleName} had member center in the past, but it does not have member center now.`)
    }
})

Then("[B2C-CSCZ] club-lang must be en-eu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-CSCZ]'
    const signInSelector = 'body > header > aside.line-1 > aside > nav > div.right-wrapper > div.log_area > a'
    await this.page.waitForSelector(signInSelector)
    await this.page.click(signInSelector)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    await this.page.waitForSelector('#AccountLogin')
    const memberCenterClublangurl = await this.page.url()
    const clubExpect = "club.benq.eu"
    const clubActual = memberCenterClublangurl.slice(8,20)
    const clubLangExpect = "lang=en-eu"
    const clubLangActual = memberCenterClublangurl.slice(71,81)
    const clubActualCheck = memberCenterClublangurl.indexOf(clubExpect)
    const clubLangActualCheck = memberCenterClublangurl.indexOf(clubLangExpect)
    if(clubActualCheck <0 && clubLangActualCheck <0 ){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect} and club-lang is not ${clubLangExpect} , member center URL includes ${clubActual} and club-lang is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubActualCheck<0){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect}, it is ${clubActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubLangActualCheck <0 ){
        throw new Error(`${titleName} club-lang is not ${clubLangExpect}, it is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
})

Then("[B2C-CSCZ] EC hostname must be shop.benq.eu && EC URL must be eu-buy",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-CSCZ]'
    const hostnameExpect = "shop.benq.eu"
    const productInfoUrl = "https://www.benq.eu/cs-cz/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
    //console.log(productInfoJson)
    //如果以前有EC但現在沒了EC
    const noEC = []
    for(let i =0; i<productInfoJson.length ; i++){
        const noECCheck  = productInfoJson[i].checkProduct
        const noEcUrlBuy = ""//以前如果沒EC的話就是空的
        if(noECCheck ==noEcUrlBuy || noECCheck.indexOf("eu-buy")<0){
            noEC.push(noECCheck)
        }
    }
    //console.log("noEC:",noEC)
    if(noEC.length === productInfoJson.length){
        throw new Error(`${titleName}  had EC in the past, but now it did not have EC now.`)
    }
    for(let i =0; i<productInfoJson.length ; i++){
        const ecCheck = productInfoJson[i].checkProduct
        const ecHostName = productInfoJson[i].buyNowlink
        if(ecCheck !== "" && ecHostName.indexOf("-buy")>0 && ecHostName.indexOf(hostnameExpect)<0 && ecCheck.indexOf("www.benq.eu")<0){
            //CS-CZ有一個Buy Now Link是us-buy
            //https://www.benq.eu/cs-cz/monitor/accessory-software/as10.html
            //https://buy.benq.com/us-buy/as10-single-arm-monitor-stand.html
            const hostNameActual = ecHostName.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  EC Host Name is not ${hostnameExpect}, it is ${hostNameActual} now. Here is the URL ${ecHostName}`)
        }
    }
    const ecUrlBuyExpect = "eu-buy"
    for(let i =0; i<productInfoJson.length ; i++){
        const ecCheck = productInfoJson[i].checkProduct
        const ecUrl= productInfoJson[i].buyNowlink
        //console.log(ecUrl)
        if(ecCheck !== "" && ecCheck.indexOf("-buy")>0 && ecCheck.indexOf(ecUrlBuyExpect)<0){
            const ecUrlBuyActual = ecCheck.slice(21,27) //xx-buy
            throw new Error(`${titleName}  EC Host Name is not ${ecUrlBuyExpect}, it is ${ecUrlBuyActual} now. Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-ENDK Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/en-dk/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-ENDK] B2B URL must be en-eu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENDK]'
    const b2bNameExpect = 'en-eu'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

When("[B2C-ENDK] Go to Member Center",{timeout: 12 * 5000},async function(){
    const titleName = '[B2C-ENDK]'
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    if(memberCenterCheck <0){
        throw new Error(`${titleName} had member center in the past, but it does not have member center now.`)
    }
})

Then("[B2C-ENDK] club-lang must be en-dk",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENDK]'
    const signInSelector = 'body > header > aside.line-1 > aside > nav > div.right-wrapper > div.log_area > a'
    await this.page.waitForSelector(signInSelector)
    await this.page.click(signInSelector)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    await this.page.waitForSelector('#AccountLogin')
    const memberCenterClublangurl = await this.page.url()
    // https://club.benq.eu/ICDS_EU/Home/BenQAuth?system_id=G5&function=Login&lang=en-dk&return_url=https%3A%2F%2Fwww.benq.eu%2Fen-dk%2Findex.login.html
    const clubExpect = "club.benq.eu"
    const clubActual = memberCenterClublangurl.slice(8,20)
    const clubLangExpect = "lang=en-dk"
    const clubLangActual = memberCenterClublangurl.slice(71,81)
    const clubActualCheck = memberCenterClublangurl.indexOf(clubExpect)
    const clubLangActualCheck = memberCenterClublangurl.indexOf(clubLangExpect)
    if(clubActualCheck <0 && clubLangActualCheck <0 ){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect} and club-lang is not ${clubLangExpect} , member center URL includes ${clubActual} and club-lang is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubActualCheck<0){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect}, it is ${clubActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubLangActualCheck <0 ){
        throw new Error(`${titleName} club-lang is not ${clubLangExpect}, it is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
})

Then("[B2C-ENDK] EC hostname must be shop.benq.eu && EC URL must be sc-buy",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENDK]'
    const hostnameExpect = "shop.benq.eu"
    const productInfoUrl = "https://www.benq.eu/en-dk/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
    //console.log(productInfoJson)
    //如果以前有EC但現在沒了EC
    const noEC = []
    for(let i =0; i<productInfoJson.length ; i++){
        const noECCheck = productInfoJson[i].checkProduct
        const noEcUrlBuy = ""//以前如果沒EC的話就是空的
        if(noECCheck ==noEcUrlBuy || noECCheck.indexOf("sc-buy")<0){
            noEC.push(noECCheck)
        }
    }
    // console.log("noEC:",noEC)
    if(noEC.length === productInfoJson.length){
        throw new Error(`${titleName}  had EC in the past, but now it did not have EC now.`)
    }
    for(let i =0; i<productInfoJson.length ; i++){
        const ecCheck = productInfoJson[i].checkProduct
        const ecHostName = productInfoJson[i].buyNowlink
        if(ecCheck !== "" && ecHostName.indexOf("-buy")>0 && ecHostName.indexOf(hostnameExpect)<0){
            const hostNameActual = ecHostName.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  EC Host Name is not ${hostnameExpect}, it is ${hostNameActual} now. Here is the URL ${ecHostName}`)
        }
    }
    const ecUrlBuyExpect = "sc-buy"//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecCheck = productInfoJson[i].checkProduct
        const ecUrl= productInfoJson[i].buyNowlink
        //console.log(ecUrl)
        if(ecCheck !== "" && ecCheck.indexOf("-buy")>0 && ecCheck.indexOf(ecUrlBuyExpect)<0){
            const ecUrlBuyActual = ecCheck.slice(21,27) //xx-buy
            throw new Error(`${titleName}  EC Host Name is not ${ecUrlBuyExpect}, it is ${ecUrlBuyActual} now. Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-ENUK Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/en-uk/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-ENUK] B2B URL must be en-uk",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENUK]'
    const b2bNameExpect = 'en-uk'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

When("[B2C-ENUK] Go to Member Center",{timeout: 12 * 5000},async function(){
    const titleName = '[B2C-ENUK]'
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    if(memberCenterCheck <0){
        throw new Error(`${titleName} had member center in the past, but it does not have member center now.`)
    }
})

Then("[B2C-ENUK] club-lang must be en-uk",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENUK]'
    const signInSelector = 'body > header > aside.line-1 > aside > nav > div.right-wrapper > div.log_area > a'
    await this.page.waitForSelector(signInSelector)
    await this.page.click(signInSelector)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    await this.page.waitForSelector('#AccountLogin')
    const memberCenterClublangurl = await this.page.url()
    //https://club.benq.eu/ICDS_EU/Home/BenQAuth?system_id=G5&function=Login&lang=en-eu&return_url=https%3A%2F%2Fwww.benq.eu%2Fen-eu%2Findex.login.html
    const clubExpect = "club.benq.eu"
    const clubActual = memberCenterClublangurl.slice(8,20)
    const clubLangExpect = "lang=en-uk"
    const clubLangActual = memberCenterClublangurl.slice(71,81)
    const clubActualCheck = memberCenterClublangurl.indexOf(clubExpect)
    const clubLangActualCheck = memberCenterClublangurl.indexOf(clubLangExpect)
    if(clubActualCheck <0 && clubLangActualCheck <0 ){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect} and club-lang is not ${clubLangExpect} , member center URL includes ${clubActual} and club-lang is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubActualCheck<0){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect}, it is ${clubActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubLangActualCheck <0 ){
        throw new Error(`${titleName} club-lang is not ${clubLangExpect}, it is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
})

Then("[B2C-ENUK] EC hostname must be shop.benq.eu & EC URL must be uk-buy",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENUK]'
    const hostnameExpect = "shop.benq.eu"
    const productInfoUrl = "https://www.benq.eu/en-uk/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
    //console.log(productInfoJson)
    //如果以前有EC但現在沒了EC
    const noEC = []
    for(let i =0; i<productInfoJson.length ; i++){
        const noECCheck = productInfoJson[i].checkProduct
        const noEcUrlBuy = ""//以前如果沒EC的話就是空的
        if(noECCheck ==noEcUrlBuy || noECCheck.indexOf("uk-buy")<0){
            noEC.push(noECCheck)
        }
    }
    //console.log("noEC:",noEC)
    if(noEC.length === productInfoJson.length){
        throw new Error(`${titleName}  had EC in the past, but now it did not have EC now.`)
    }
    for(let i =0; i<productInfoJson.length ; i++){
        const ecCheck = productInfoJson[i].checkProduct
        const ecHostName = productInfoJson[i].buyNowlink
        if(ecCheck !== "" && ecHostName.indexOf("-buy")>0 && ecHostName.indexOf(hostnameExpect)<0){
            const hostNameActual = ecHostName.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  EC Host Name is not ${hostnameExpect}, it is ${hostNameActual} now. Here is the URL ${ecHostName}`)
        }
    }
    const ecUrlBuyExpect = "uk-buy"
    for(let i =0; i<productInfoJson.length ; i++){
        const ecCheck = productInfoJson[i].checkProduct
        const ecUrl= productInfoJson[i].buyNowlink
        //console.log(ecUrl)
        if(ecCheck !== "" && ecCheck.indexOf("-buy")>0 && ecCheck.indexOf(ecUrlBuyExpect)<0){
            const ecUrlBuyActual = ecCheck.slice(21,27) //xx-buy
            throw new Error(`${titleName}  EC Host Name is not ${ecUrlBuyExpect}, it is ${ecUrlBuyActual} now. Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-ENIE Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/en-ie/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-ENIE] B2B URL must be en-uk",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENIE]'
    const b2bNameExpect = 'en-uk'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

When("[B2C-ENIE] Go to Member Center",{timeout: 12 * 5000},async function(){
    const titleName = '[B2C-ENIE]'
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    if(memberCenterCheck <0){
        throw new Error(`${titleName} had member center in the past, but it does not have member center now.`)
    }
})

Then("[B2C-ENIE] club-lang must be en-ie",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENIE]'
    const signInSelector = 'body > header > aside.line-1 > aside > nav > div.right-wrapper > div.log_area > a'
    await this.page.waitForSelector(signInSelector)
    await this.page.click(signInSelector)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    await this.page.waitForSelector('#AccountLogin')
    const memberCenterClublangurl = await this.page.url()
    //https://club.benq.eu/ICDS_EU/Home/BenQAuth?system_id=G5&function=Login&lang=en-eu&return_url=https%3A%2F%2Fwww.benq.eu%2Fen-eu%2Findex.login.html
    const clubExpect = "club.benq.eu"
    const clubActual = memberCenterClublangurl.slice(8,20)
    const clubLangExpect = "lang=en-ie"
    const clubLangActual = memberCenterClublangurl.slice(71,81)
    const clubActualCheck = memberCenterClublangurl.indexOf(clubExpect)
    const clubLangActualCheck = memberCenterClublangurl.indexOf(clubLangExpect)
    if(clubActualCheck <0 && clubLangActualCheck <0 ){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect} and club-lang is not ${clubLangExpect} , member center URL includes ${clubActual} and club-lang is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubActualCheck<0){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect}, it is ${clubActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubLangActualCheck <0 ){
        throw new Error(`${titleName} club-lang is not ${clubLangExpect}, it is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
})

Then("[B2C-ENIE] EC hostname must be shop.benq.eu & EC URL must be ie-buy",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENIE]'
    const hostnameExpect = "shop.benq.eu"
    const productInfoUrl = "https://www.benq.eu/en-ie/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
    //console.log(productInfoJson)
    //如果以前有EC但現在沒了EC
    const noEC = []
    for(let i =0; i<productInfoJson.length ; i++){
        const noECCheck = productInfoJson[i].checkProduct
        const noEcUrlBuy = ""//以前如果沒EC的話就是空的
        if(noECCheck ==noEcUrlBuy || noECCheck.indexOf("ie-buy")<0){
            noEC.push(noECCheck)
        }
    }
    //console.log("noEC:",noEC)
    if(noEC.length === productInfoJson.length){
        throw new Error(`${titleName}  had EC in the past, but now it did not have EC now.`)
    }
    for(let i =0; i<productInfoJson.length ; i++){
        const ecCheck = productInfoJson[i].checkProduct
        const ecHostName = productInfoJson[i].buyNowlink
        if(ecCheck !== "" && ecHostName.indexOf("-buy")>0 && ecHostName.indexOf(hostnameExpect)<0){
            const hostNameActual = ecHostName.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  EC Host Name is not ${hostnameExpect}, it is ${hostNameActual} now. Here is the URL ${ecHostName}`)
        }
    }
    const ecUrlBuyExpect = "ie-buy"
    for(let i =0; i<productInfoJson.length ; i++){
        const ecCheck = productInfoJson[i].checkProduct
        const ecUrl= productInfoJson[i].buyNowlink
        //console.log(ecUrl)
        if(ecCheck !== "" && ecCheck.indexOf("-buy")>0 && ecCheck.indexOf(ecUrlBuyExpect)<0){
            const ecUrlBuyActual = ecCheck.slice(21,27) //xx-buy
            throw new Error(`${titleName}  EC Host Name is not ${ecUrlBuyExpect}, it is ${ecUrlBuyActual} now. Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-ENLU Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/en-lu/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-ENLU] B2B URL must be en-eu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENLU]'
    const b2bNameExpect = 'en-eu'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

When("[B2C-ENLU] Go to Member Center",{timeout: 12 * 5000},async function(){
    const titleName = '[B2C-ENLU]'
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    if(memberCenterCheck <0){
        throw new Error(`${titleName} had member center in the past, but it does not have member center now.`)
    }
})

Then("[B2C-ENLU] club-lang must be en-lu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENLU]'
    const signInSelector = 'body > header > aside.line-1 > aside > nav > div.right-wrapper > div.log_area > a'
    await this.page.waitForSelector(signInSelector)
    await this.page.click(signInSelector)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    await this.page.waitForSelector('#AccountLogin')
    const memberCenterClublangurl = await this.page.url()
    //https://club.benq.eu/ICDS_EU/Home/BenQAuth?system_id=G5&function=Login&lang=en-eu&return_url=https%3A%2F%2Fwww.benq.eu%2Fen-eu%2Findex.login.html
    const clubExpect = "club.benq.eu"
    const clubActual = memberCenterClublangurl.slice(8,20)
    const clubLangExpect = "lang=en-lu"
    const clubLangActual = memberCenterClublangurl.slice(71,81)
    const clubActualCheck = memberCenterClublangurl.indexOf(clubExpect)
    const clubLangActualCheck = memberCenterClublangurl.indexOf(clubLangExpect)
    if(clubActualCheck <0 && clubLangActualCheck <0 ){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect} and club-lang is not ${clubLangExpect} , member center URL includes ${clubActual} and club-lang is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubActualCheck<0){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect}, it is ${clubActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubLangActualCheck <0 ){
        throw new Error(`${titleName} club-lang is not ${clubLangExpect}, it is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
})

Then("[B2C-ENLU] EC hostname must be shop.benq.eu & EC URL must be eu-buy",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENLU]'
    const hostnameExpect = "shop.benq.eu"
    const productInfoUrl = "https://www.benq.eu/en-lu/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
    //console.log(productInfoJson)
    //如果以前有EC但現在沒了EC
    const noEC = []
    for(let i =0; i<productInfoJson.length ; i++){
        const noECCheck = productInfoJson[i].checkProduct
        const noEcUrlBuy = ""//以前如果沒EC的話就是空的
        if(noECCheck ==noEcUrlBuy || noECCheck.indexOf("eu-buy")<0){
            noEC.push(noECCheck)
        }
    }
    //console.log("noEC:",noEC)
    if(noEC.length === productInfoJson.length){
        throw new Error(`${titleName}  had EC in the past, but now it did not have EC now.`)
    }
    for(let i =0; i<productInfoJson.length ; i++){
        const ecCheck = productInfoJson[i].checkProduct
        const ecHostName = productInfoJson[i].buyNowlink
        if(ecCheck !== "" && ecHostName.indexOf("-buy")>0 && ecHostName.indexOf(hostnameExpect)<0){
            const hostNameActual = ecHostName.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  EC Host Name is not ${hostnameExpect}, it is ${hostNameActual} now. Here is the URL ${ecHostName}`)
        }
    }
    const ecUrlBuyExpect = "eu-buy"
    for(let i =0; i<productInfoJson.length ; i++){
        const ecCheck = productInfoJson[i].checkProduct
        const ecUrl= productInfoJson[i].buyNowlink
        //console.log(ecUrl)
        if(ecCheck !== "" && ecCheck.indexOf("-buy")>0 && ecCheck.indexOf(ecUrlBuyExpect)<0){
            const ecUrlBuyActual = ecCheck.slice(21,27) //xx-buy
            throw new Error(`${titleName}  EC Host Name is not ${ecUrlBuyExpect}, it is ${ecUrlBuyActual} now. Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-SVSE Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/sv-se/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-SVSE] B2B URL must be en-eu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-SVSE]'
    const b2bNameExpect = 'en-eu'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

When("[B2C-SVSE] Go to Member Center",{timeout: 12 * 5000},async function(){
    const titleName = '[B2C-SVSE]'
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    if(memberCenterCheck <0){
        throw new Error(`${titleName} had member center in the past, but it does not have member center now.`)
    }
})

Then("[B2C-SVSE] club-lang must be sv-se",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-SVSE]'
    const signInSelector = 'body > header > aside.line-1 > aside > nav > div.right-wrapper > div.log_area > a'
    await this.page.waitForSelector(signInSelector)
    await this.page.click(signInSelector)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    await this.page.waitForSelector('#AccountLogin')
    const memberCenterClublangurl = await this.page.url()
    //https://club.benq.eu/ICDS_EU/Home/BenQAuth?system_id=G5&function=Login&lang=en-eu&return_url=https%3A%2F%2Fwww.benq.eu%2Fen-eu%2Findex.login.html
    const clubExpect = "club.benq.eu"
    const clubActual = memberCenterClublangurl.slice(8,20)
    const clubLangExpect = "lang=sv-se"
    const clubLangActual = memberCenterClublangurl.slice(71,81)
    const clubActualCheck = memberCenterClublangurl.indexOf(clubExpect)
    const clubLangActualCheck = memberCenterClublangurl.indexOf(clubLangExpect)
    if(clubActualCheck <0 && clubLangActualCheck <0 ){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect} and club-lang is not ${clubLangExpect} , member center URL includes ${clubActual} and club-lang is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubActualCheck<0){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect}, it is ${clubActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubLangActualCheck <0 ){
        throw new Error(`${titleName} club-lang is not ${clubLangExpect}, it is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
})

Then("[B2C-SVSE] EC hostname must be shop.benq.eu & EC URL must be sc-buy",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-SVSE]'
    const hostnameExpect = "shop.benq.eu"
    const productInfoUrl = "https://www.benq.eu/sv-se/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
    //console.log(productInfoJson)
    //如果以前有EC但現在沒了EC
    const noEC = []
    for(let i =0; i<productInfoJson.length ; i++){
        const noECCheck = productInfoJson[i].checkProduct
        const noEcUrlBuy = ""//以前如果沒EC的話就是空的
        if(noECCheck ==noEcUrlBuy || noECCheck.indexOf("eu-buy")<0){
            noEC.push(noECCheck)
        }
    }
    //console.log("noEC:",noEC)
    if(noEC.length === productInfoJson.length){
        throw new Error(`${titleName}  had EC in the past, but now it did not have EC now.`)
    }
    for(let i =0; i<productInfoJson.length ; i++){
        const ecCheck = productInfoJson[i].checkProduct
        const ecHostName = productInfoJson[i].buyNowlink
        if(ecCheck !== "" && ecHostName.indexOf("-buy")>0 && ecHostName.indexOf(hostnameExpect)<0){
            const hostNameActual = ecHostName.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  EC Host Name is not ${hostnameExpect}, it is ${hostNameActual} now. Here is the URL ${ecHostName}`)
        }
    }
    const ecUrlBuyExpect = "sc-buy"
    for(let i =0; i<productInfoJson.length ; i++){
        const ecCheck = productInfoJson[i].checkProduct
        const ecUrl= productInfoJson[i].buyNowlink
        //console.log(ecUrl)
        if(ecCheck !== "" && ecCheck.indexOf("-buy")>0 && ecCheck.indexOf(ecUrlBuyExpect)<0){
            const ecUrlBuyActual = ecCheck.slice(21,27) //xx-buy
            throw new Error(`${titleName}  EC Host Name is not ${ecUrlBuyExpect}, it is ${ecUrlBuyActual} now. Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-ENCEE Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/en-cee/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-ENCEE] B2B URL must be en-eu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENCEE]'
    const b2bNameExpect = 'en-eu'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

Then("[B2C-ENCEE] B2C-ENCEE Page does not have Member Center",{timeout: 124 * 5000},async function(){
    const titleName = '[B2C-ENCEE]'
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    //目前en-ap沒有member center
    //以前沒有member center但現在有的話
    if(memberCenterCheck > 0){
        throw new Error(`${titleName}  did not have member center in the past, but it have member center now.`)
    }
})

Then("[B2C-ENCEE] B2C-ENCEE Page does not have EC Hostname and URL",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENCEE]'
    const productInfoUrl = "https://www.benq.eu/en-cee/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
        //console.log(productInfoJson)
    const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].buyNowlink
        const ecCheck = productInfoJson[i].checkProduct
        //console.log(ecUrl)
        if(ecCheck !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
            //因為ENAP有些buyNowlink是us-buy, 官網上沒開EC, 但是Product Json上有
            //https://www.benq.com/en-ap/projector/accessory/carrybag-ql01.html
            //buyNowlink: "https://buy.benq.com/us-buy/soft-carrying-case-for-ht3550-tk850-tk810-tk800m-ht2150st-mh750-mh760-ht2050-ht3050-ht2550-tk800.html"
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-FRFR Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/fr-fr/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-FRFR] B2B URL must be fr-fr",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-FRFR]'
    const b2bNameExpect = 'fr-fr'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

When("[B2C-FRFR] Go to Member Center",{timeout: 12 * 5000},async function(){
    const titleName = '[B2C-FRFR]'
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    if(memberCenterCheck <0){
        throw new Error(`${titleName} had member center in the past, but it does not have member center now.`)
    }
})

Then("[B2C-FRFR] club-lang must be fr-fr",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-FRFR]'
    const signInSelector = 'body > header > aside.line-1 > aside > nav > div.right-wrapper > div.log_area > a'
    await this.page.waitForSelector(signInSelector)
    await this.page.click(signInSelector)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    await this.page.waitForSelector('#AccountLogin')
    const memberCenterClublangurl = await this.page.url()
    //https://club.benq.eu/ICDS_EU/Home/BenQAuth?system_id=G5&function=Login&lang=en-eu&return_url=https%3A%2F%2Fwww.benq.eu%2Fen-eu%2Findex.login.html
    const clubExpect = "club.benq.eu"
    const clubActual = memberCenterClublangurl.slice(8,20)
    const clubLangExpect = "lang=fr-fr"
    const clubLangActual = memberCenterClublangurl.slice(71,81)
    const clubActualCheck = memberCenterClublangurl.indexOf(clubExpect)
    const clubLangActualCheck = memberCenterClublangurl.indexOf(clubLangExpect)
    if(clubActualCheck <0 && clubLangActualCheck <0 ){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect} and club-lang is not ${clubLangExpect} , member center URL includes ${clubActual} and club-lang is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubActualCheck<0){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect}, it is ${clubActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubLangActualCheck <0 ){
        throw new Error(`${titleName} club-lang is not ${clubLangExpect}, it is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
})

Then("[B2C-FRFR] EC hostname must be shop.benq.eu & EC URL must be fr-buy",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-FRFR]'
    const hostnameExpect = "shop.benq.eu"
    const productInfoUrl = "https://www.benq.eu/fr-fr/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
    //console.log(productInfoJson)
    //如果以前有EC但現在沒了EC
    const noEC = []
    for(let i =0; i<productInfoJson.length ; i++){
        const noECCheck  = productInfoJson[i].checkProduct
        const noEcUrlBuy = ""//以前如果沒EC的話就是空的
        if(noECCheck ==noEcUrlBuy || noECCheck.indexOf("fr-buy")<0){
            noEC.push(noECCheck)
        }
    }
    //console.log("noEC:",noEC)
    if(noEC.length === productInfoJson.length){
        throw new Error(`${titleName}  had EC in the past, but now it did not have EC now.`)
    }
    for(let i =0; i<productInfoJson.length ; i++){
        const ecCheck = productInfoJson[i].checkProduct
        const ecHostName = productInfoJson[i].buyNowlink
        if(ecCheck !== "" && ecHostName.indexOf("-buy")>0 && ecHostName.indexOf(hostnameExpect)<0){
            const hostNameActual = ecHostName.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  EC Host Name is not ${hostnameExpect}, it is ${hostNameActual} now. Here is the URL ${ecHostName}`)
        }
    }
    const ecUrlBuyExpect = "fr-buy"
    for(let i =0; i<productInfoJson.length ; i++){
        const ecCheck = productInfoJson[i].checkProduct
        const ecUrl= productInfoJson[i].buyNowlink
        //console.log(ecUrl)
        if(ecCheck !== "" && ecCheck.indexOf("-buy")>0 && ecCheck.indexOf(ecUrlBuyExpect)<0){
            const ecUrlBuyActual = ecCheck.slice(21,27) //xx-buy
            throw new Error(`${titleName}  EC Host Name is not ${ecUrlBuyExpect}, it is ${ecUrlBuyActual} now. Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-FRCH Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/fr-ch/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-FRCH] B2B URL must be fr-fr",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-FRCH]'
    const b2bNameExpect = 'fr-fr'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

When("[B2C-FRCH] Go to Member Center",{timeout: 12 * 5000},async function(){
    const titleName = '[B2C-FRCH]'
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    if(memberCenterCheck <0){
        throw new Error(`${titleName} had member center in the past, but it does not have member center now.`)
    }
})

Then("[B2C-FRCH] club-lang must be fr-ch",{timeout: 124 * 5000},async function(){
    const titleName = '[B2C-FRCH]'
    const signInSelector = 'body > header > aside.line-1 > aside > nav > div.right-wrapper > div.log_area > a'
    await this.page.waitForSelector(signInSelector)
    await this.page.click(signInSelector)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    await this.page.waitForSelector('#AccountLogin')
    const memberCenterClublangurl = await this.page.url()
    //https://club.benq.eu/ICDS_EU/Home/BenQAuth?system_id=G5&function=Login&lang=en-eu&return_url=https%3A%2F%2Fwww.benq.eu%2Fen-eu%2Findex.login.html
    const clubExpect = "club.benq.eu"
    const clubActual = memberCenterClublangurl.slice(8,20)
    const clubLangExpect = "lang=fr-ch"
    const clubLangActual = memberCenterClublangurl.slice(71,81)
    const clubActualCheck = memberCenterClublangurl.indexOf(clubExpect)
    const clubLangActualCheck = memberCenterClublangurl.indexOf(clubLangExpect)
    if(clubActualCheck <0 && clubLangActualCheck <0 ){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect} and club-lang is not ${clubLangExpect} , member center URL includes ${clubActual} and club-lang is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubActualCheck<0){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect}, it is ${clubActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubLangActualCheck <0 ){
        throw new Error(`${titleName} club-lang is not ${clubLangExpect}, it is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
})

Then("[B2C-FRCH] B2C-FRCH Page does not have EC Hostname and URL",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-FRCH]'
    const productInfoUrl = "https://www.benq.eu/fr-ch/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
        //console.log(productInfoJson)
    const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].buyNowlink
        const ecCheck = productInfoJson[i].checkProduct
        //console.log(ecUrl)
        if(ecCheck !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
            //因為ENAP有些buyNowlink是us-buy, 官網上沒開EC, 但是Product Json上有
            //https://www.benq.com/en-ap/projector/accessory/carrybag-ql01.html
            //buyNowlink: "https://buy.benq.com/us-buy/soft-carrying-case-for-ht3550-tk850-tk810-tk800m-ht2150st-mh750-mh760-ht2050-ht3050-ht2550-tk800.html"
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-ESES Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/es-es/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-ESES] B2B URL must be es-es",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ESES]'
    const b2bNameExpect = 'es-es'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

When("[B2C-ESES] Go to Member Center",{timeout: 12 * 5000},async function(){
    const titleName = '[B2C-ESES]'
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    if(memberCenterCheck <0){
        throw new Error(`${titleName} had member center in the past, but it does not have member center now.`)
    }
})

Then("[B2C-ESES] club-lang must be es-es",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ESES]'
    const signInSelector = 'body > header > aside.line-1 > aside > nav > div.right-wrapper > div.log_area > a'
    await this.page.waitForSelector(signInSelector)
    await this.page.click(signInSelector)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    await this.page.waitForSelector('#AccountLogin')
    const memberCenterClublangurl = await this.page.url()
    //https://club.benq.eu/ICDS_EU/Home/BenQAuth?system_id=G5&function=Login&lang=en-eu&return_url=https%3A%2F%2Fwww.benq.eu%2Fen-eu%2Findex.login.html
    const clubExpect = "club.benq.eu"
    const clubActual = memberCenterClublangurl.slice(8,20)
    const clubLangExpect = "lang=es-es"
    const clubLangActual = memberCenterClublangurl.slice(71,81)
    const clubActualCheck = memberCenterClublangurl.indexOf(clubExpect)
    const clubLangActualCheck = memberCenterClublangurl.indexOf(clubLangExpect)
    if(clubActualCheck <0 && clubLangActualCheck <0 ){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect} and club-lang is not ${clubLangExpect} , member center URL includes ${clubActual} and club-lang is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubActualCheck<0){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect}, it is ${clubActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubLangActualCheck <0 ){
        throw new Error(`${titleName} club-lang is not ${clubLangExpect}, it is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
})

Then("[B2C-ESES] EC hostname must be shop.benq.eu & EC URL must be es-buy",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ESES]'
    const hostnameExpect = "shop.benq.eu"
    const productInfoUrl = "https://www.benq.eu/es-es/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
    //console.log(productInfoJson)
    //如果以前有EC但現在沒了EC
    const noEC = []
    for(let i =0; i<productInfoJson.length ; i++){
        const noECCheck = productInfoJson[i].checkProduct
        const noEcUrlBuy = ""//以前如果沒EC的話就是空的
        if(noECCheck ==noEcUrlBuy || noECCheck.indexOf("es-buy")<0){
            noEC.push(noECCheck)
        }
    }
    //console.log("noEC:",noEC)
    if(noEC.length === productInfoJson.length){
        throw new Error(`${titleName}  had EC in the past, but now it did not have EC now.`)
    }
    for(let i =0; i<productInfoJson.length ; i++){
        const ecCheck = productInfoJson[i].checkProduct
        const ecHostName = productInfoJson[i].buyNowlink
        if(ecCheck !== "" && ecHostName.indexOf("-buy")>0 && ecHostName.indexOf(hostnameExpect)<0){
            const hostNameActual = ecHostName.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  EC Host Name is not ${hostnameExpect}, it is ${hostNameActual} now. Here is the URL ${ecHostName}`)
        }
    }
    const ecUrlBuyExpect = "es-buy"
    for(let i =0; i<productInfoJson.length ; i++){
        const ecCheck = productInfoJson[i].checkProduct
        const ecUrl= productInfoJson[i].buyNowlink
        //console.log(ecUrl)
        if(ecCheck !== "" && ecCheck.indexOf("-buy")>0 && ecCheck.indexOf(ecUrlBuyExpect)<0){
            const ecUrlBuyActual = ecCheck.slice(21,27) //xx-buy
            throw new Error(`${titleName}  EC Host Name is not ${ecUrlBuyExpect}, it is ${ecUrlBuyActual} now. Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-ITIT Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/it-it/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-ITIT] B2B URL must be it-it",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ITIT]'
    const b2bNameExpect = 'it-it'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

When("[B2C-ITIT] Go to Member Center",{timeout: 12 * 5000},async function(){
    const titleName = '[B2C-ITIT]'
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    if(memberCenterCheck <0){
        throw new Error(`${titleName} had member center in the past, but it does not have member center now.`)
    }
})

Then("[B2C-ITIT] club-lang must be it-it",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ITIT]'
    const signInSelector = 'body > header > aside.line-1 > aside > nav > div.right-wrapper > div.log_area > a'
    await this.page.waitForSelector(signInSelector)
    await this.page.click(signInSelector)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    await this.page.waitForSelector('#AccountLogin')
    const memberCenterClublangurl = await this.page.url()
    //https://club.benq.eu/ICDS_EU/Home/BenQAuth?system_id=G5&function=Login&lang=en-eu&return_url=https%3A%2F%2Fwww.benq.eu%2Fen-eu%2Findex.login.html
    const clubExpect = "club.benq.eu"
    const clubActual = memberCenterClublangurl.slice(8,20)
    const clubLangExpect = "lang=it-it"
    const clubLangActual = memberCenterClublangurl.slice(71,81)
    const clubActualCheck = memberCenterClublangurl.indexOf(clubExpect)
    const clubLangActualCheck = memberCenterClublangurl.indexOf(clubLangExpect)
    if(clubActualCheck <0 && clubLangActualCheck <0 ){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect} and club-lang is not ${clubLangExpect} , member center URL includes ${clubActual} and club-lang is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubActualCheck<0){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect}, it is ${clubActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubLangActualCheck <0 ){
        throw new Error(`${titleName} club-lang is not ${clubLangExpect}, it is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
})

Then("[B2C-ITIT] EC hostname must be shop.benq.eu & EC URL must be it-buy",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ITIT]'
    const hostnameExpect = "shop.benq.eu"
    const productInfoUrl = "https://www.benq.eu/it-it/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
    //console.log(productInfoJson)
    //如果以前有EC但現在沒了EC
    const noEC = []
    for(let i =0; i<productInfoJson.length ; i++){
        const noECCheck = productInfoJson[i].checkProduct
        const noEcUrlBuy = ""//以前如果沒EC的話就是空的
        if(noECCheck ==noEcUrlBuy || noECCheck.indexOf("it-buy")<0){
            noEC.push(noECCheck)
        }
    }
    //console.log("noEC:",noEC)
    if(noEC.length === productInfoJson.length){
        throw new Error(`${titleName}  had EC in the past, but now it did not have EC now.`)
    }
    for(let i =0; i<productInfoJson.length ; i++){
        const ecCheck = productInfoJson[i].checkProduct
        const ecHostName = productInfoJson[i].buyNowlink
        if(ecCheck !== "" && ecHostName.indexOf("-buy")>0 && ecHostName.indexOf(hostnameExpect)<0){
            const hostNameActual = ecHostName.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  EC Host Name is not ${hostnameExpect}, it is ${hostNameActual} now. Here is the URL ${ecHostName}`)
        }
    }
    const ecUrlBuyExpect = "it-buy"
    for(let i =0; i<productInfoJson.length ; i++){
        const ecCheck = productInfoJson[i].checkProduct
        const ecUrl= productInfoJson[i].buyNowlink
        //console.log(ecUrl)
        if(ecCheck !== "" && ecCheck.indexOf("-buy")>0 && ecCheck.indexOf(ecUrlBuyExpect)<0){
            const ecUrlBuyActual = ecCheck.slice(21,27) //xx-buy
            throw new Error(`${titleName}  EC Host Name is not ${ecUrlBuyExpect}, it is ${ecUrlBuyActual} now. Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-PTPT Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.com/pt-pt/index.html'+cicGA)
})

Then("[B2C-PTPT] B2B URL must be en-eu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-PTPT]'
    const b2bNameExpect = 'en-eu'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

When("[B2C-PTPT] Go to Member Center",{timeout: 12 * 5000},async function(){
    const titleName = '[B2C-PTPT]'
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    if(memberCenterCheck <0){
        throw new Error(`${titleName} had member center in the past, but it does not have member center now.`)
    }
})

Then("[B2C-PTPT] club-lang must be pt-pt",{timeout: 124 * 5000},async function(){
    const titleName = '[B2C-PTPT]'
    const signInSelector = 'body > header > aside.line-1 > aside > nav > div.right-wrapper > div.log_area > a'
    await this.page.waitForSelector(signInSelector)
    await this.page.click(signInSelector)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    await this.page.waitForSelector('#AccountLogin')
    const memberCenterClublangurl = await this.page.url()
    //https://club.benq.eu/ICDS_EU/Home/BenQAuth?system_id=G5&function=Login&lang=en-eu&return_url=https%3A%2F%2Fwww.benq.eu%2Fen-eu%2Findex.login.html
    const clubExpect = "club.benq.eu"
    const clubActual = memberCenterClublangurl.slice(8,20)
    const clubLangExpect = "lang=pt-pt"
    const clubLangActual = memberCenterClublangurl.slice(71,81)
    const clubActualCheck = memberCenterClublangurl.indexOf(clubExpect)
    const clubLangActualCheck = memberCenterClublangurl.indexOf(clubLangExpect)
    if(clubActualCheck <0 && clubLangActualCheck <0 ){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect} and club-lang is not ${clubLangExpect} , member center URL includes ${clubActual} and club-lang is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubActualCheck<0){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect}, it is ${clubActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubLangActualCheck <0 ){
        throw new Error(`${titleName} club-lang is not ${clubLangExpect}, it is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
})

Then("[B2C-PTPT] B2C-PTPT Page does not have EC Hostname and URL",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-PTPT]'
    const productInfoUrl = "https://www.benq.com/pt-pt/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
        //console.log(productInfoJson)
    const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].buyNowlink
        const ecCheck = productInfoJson[i].checkProduct
        //console.log(ecUrl)
        if(ecCheck !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
            //因為ENAP有些buyNowlink是us-buy, 官網上沒開EC, 但是Product Json上有
            //https://www.benq.com/en-ap/projector/accessory/carrybag-ql01.html
            //buyNowlink: "https://buy.benq.com/us-buy/soft-carrying-case-for-ht3550-tk850-tk810-tk800m-ht2150st-mh750-mh760-ht2050-ht3050-ht2550-tk800.html"
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-DEDE Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/de-de/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-DEDE] B2B URL must be de-de",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-DEDE]'
    const b2bNameExpect = 'de-de'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

When("[B2C-DEDE] Go to Member Center",{timeout: 12 * 5000},async function(){
    const titleName = '[B2C-DEDE]'
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    if(memberCenterCheck <0){
        throw new Error(`${titleName} had member center in the past, but it does not have member center now.`)
    }
})

Then("[B2C-DEDE] club-lang must be de-de",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-DEDE]'
    const signInSelector = 'body > header > aside.line-1 > aside > nav > div.right-wrapper > div.log_area > a'
    await this.page.waitForSelector(signInSelector)
    await this.page.click(signInSelector)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    await this.page.waitForSelector('#AccountLogin')
    const memberCenterClublangurl = await this.page.url()
    //https://club.benq.eu/ICDS_EU/Home/BenQAuth?system_id=G5&function=Login&lang=en-eu&return_url=https%3A%2F%2Fwww.benq.eu%2Fen-eu%2Findex.login.html
    const clubExpect = "club.benq.eu"
    const clubActual = memberCenterClublangurl.slice(8,20)
    const clubLangExpect = "lang=de-de"
    const clubLangActual = memberCenterClublangurl.slice(71,81)
    const clubActualCheck = memberCenterClublangurl.indexOf(clubExpect)
    const clubLangActualCheck = memberCenterClublangurl.indexOf(clubLangExpect)
    if(clubActualCheck <0 && clubLangActualCheck <0 ){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect} and club-lang is not ${clubLangExpect} , member center URL includes ${clubActual} and club-lang is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubActualCheck<0){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect}, it is ${clubActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubLangActualCheck <0 ){
        throw new Error(`${titleName} club-lang is not ${clubLangExpect}, it is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
})

Then("[B2C-DEDE] EC hostname must be shop.benq.eu & EC URL must be de-buy",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-DEDE]'
    const hostnameExpect = "shop.benq.eu"
    const productInfoUrl = "https://www.benq.eu/de-de/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
    //console.log(productInfoJson)
    //如果以前有EC但現在沒了EC
    const noEC = []
    for(let i =0; i<productInfoJson.length ; i++){
        const noECCheck  = productInfoJson[i].checkProduct
        const noEcUrlBuy = ""//以前如果沒EC的話就是空的
        if(noECCheck ==noEcUrlBuy || noECCheck.indexOf("de-buy")<0){
            noEC.push(noECCheck)
        }
    }
    //console.log("noEC:",noEC)
    if(noEC.length === productInfoJson.length){
        throw new Error(`${titleName}  had EC in the past, but now it did not have EC now.`)
    }
    for(let i =0; i<productInfoJson.length ; i++){
        const ecCheck = productInfoJson[i].checkProduct
        const ecHostName = productInfoJson[i].buyNowlink
        if(ecCheck !== "" && ecHostName.indexOf("-buy")>0 && ecHostName.indexOf(hostnameExpect)<0){
            const hostNameActual = ecHostName.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  EC Host Name is not ${hostnameExpect}, it is ${hostNameActual} now. Here is the URL ${ecHostName}`)
        }
    }
    const ecUrlBuyExpect = "de-buy"
    for(let i =0; i<productInfoJson.length ; i++){
        const ecCheck = productInfoJson[i].checkProduct
        const ecUrl= productInfoJson[i].buyNowlink
        //console.log(ecUrl)
        if(ecCheck !== "" && ecCheck.indexOf("-buy")>0 && ecCheck.indexOf(ecUrlBuyExpect)<0){
            const ecUrlBuyActual = ecCheck.slice(21,27) //xx-buy
            throw new Error(`${titleName}  EC Host Name is not ${ecUrlBuyExpect}, it is ${ecUrlBuyActual} now. Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-DECH Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/de-ch/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-DECH] B2B URL must be de-de",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-DECH]'
    const b2bNameExpect = 'de-de'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

Then("[B2C-DECH] B2C-DECH Page does not have Member Center",{timeout: 124 * 5000},async function(){
    const titleName = '[B2C-DECH]'
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    //目前en-ap沒有member center
    //以前沒有member center但現在有的話
    if(memberCenterCheck > 0){
        throw new Error(`${titleName}  did not have member center in the past, but it have member center now.`)
    }
})

Then("[B2C-DECH] B2C-DECH Page does not have EC Hostname and URL",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-DECH]'
    const productInfoUrl = "https://www.benq.eu/de-ch/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
        //console.log(productInfoJson)
    const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].buyNowlink
        const ecCheck = productInfoJson[i].checkProduct
        //console.log(ecUrl)
        if(ecCheck !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
            //因為ENAP有些buyNowlink是us-buy, 官網上沒開EC, 但是Product Json上有
            //https://www.benq.com/en-ap/projector/accessory/carrybag-ql01.html
            //buyNowlink: "https://buy.benq.com/us-buy/soft-carrying-case-for-ht3550-tk850-tk810-tk800m-ht2150st-mh750-mh760-ht2050-ht3050-ht2550-tk800.html"
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-NLNL Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/nl-nl/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-NLNL] B2B URL must be nl-nl",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-NLNL]'
    const b2bNameExpect = 'nl-nl'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

When("[B2C-NLNL] Go to Member Center",{timeout: 12 * 5000},async function(){
    const titleName = '[B2C-NLNL]'
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    if(memberCenterCheck <0){
        throw new Error(`${titleName} had member center in the past, but it does not have member center now.`)
    }
})

Then("[B2C-NLNL] club-lang must be nl-nl",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-NLNL]'
    const signInSelector = 'body > header > aside.line-1 > aside > nav > div.right-wrapper > div.log_area > a'
    await this.page.waitForSelector(signInSelector)
    await this.page.click(signInSelector)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    await this.page.waitForSelector('#AccountLogin')
    const memberCenterClublangurl = await this.page.url()
    //https://club.benq.eu/ICDS_EU/Home/BenQAuth?system_id=G5&function=Login&lang=en-eu&return_url=https%3A%2F%2Fwww.benq.eu%2Fen-eu%2Findex.login.html
    const clubExpect = "club.benq.eu"
    const clubActual = memberCenterClublangurl.slice(8,20)
    const clubLangExpect = "lang=nl-nl"
    const clubLangActual = memberCenterClublangurl.slice(71,81)
    const clubActualCheck = memberCenterClublangurl.indexOf(clubExpect)
    const clubLangActualCheck = memberCenterClublangurl.indexOf(clubLangExpect)
    if(clubActualCheck <0 && clubLangActualCheck <0 ){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect} and club-lang is not ${clubLangExpect} , member center URL includes ${clubActual} and club-lang is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubActualCheck<0){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect}, it is ${clubActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubLangActualCheck <0 ){
        throw new Error(`${titleName} club-lang is not ${clubLangExpect}, it is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
})

Then("[B2C-NLNL] EC hostname must be shop.benq.eu & EC URL must be nl-buy",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-NLNL]'
    const hostnameExpect = "shop.benq.eu"
    const productInfoUrl = "https://www.benq.eu/nl-nl/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
    //console.log(productInfoJson)
    //如果以前有EC但現在沒了EC
    const noEC = []
    for(let i =0; i<productInfoJson.length ; i++){
        const noECCheck  = productInfoJson[i].checkProduct
        const noEcUrlBuy = ""//以前如果沒EC的話就是空的
        if(noECCheck ==noEcUrlBuy || noECCheck.indexOf("nl-buy")<0){
            noEC.push(noECCheck)
        }
    }
    //console.log("noEC:",noEC)
    if(noEC.length === productInfoJson.length){
        throw new Error(`${titleName}  had EC in the past, but now it did not have EC now.`)
    }
    for(let i =0; i<productInfoJson.length ; i++){
        const ecCheck = productInfoJson[i].checkProduct
        const ecHostName = productInfoJson[i].buyNowlink
        if(ecCheck !== "" && ecHostName.indexOf("-buy")>0 && ecHostName.indexOf(hostnameExpect)<0){
            const hostNameActual = ecHostName.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  EC Host Name is not ${hostnameExpect}, it is ${hostNameActual} now. Here is the URL ${ecHostName}`)
        }
    }
    const ecUrlBuyExpect = "nl-buy"
    for(let i =0; i<productInfoJson.length ; i++){
        const ecCheck = productInfoJson[i].checkProduct
        const ecUrl= productInfoJson[i].buyNowlink
        //console.log(ecUrl)
        if(ecCheck !== "" && ecCheck.indexOf("-buy")>0 && ecCheck.indexOf(ecUrlBuyExpect)<0){
            const ecUrlBuyActual = ecCheck.slice(21,27) //xx-buy
            throw new Error(`${titleName}  EC Host Name is not ${ecUrlBuyExpect}, it is ${ecUrlBuyActual} now. Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-NLBE Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/nl-be/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-NLBE] B2B URL must be nl-nl",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-NLBE]'
    const b2bNameExpect = 'nl-nl'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

When("[B2C-NLBE] Go to Member Center",{timeout: 12 * 5000},async function(){
    const titleName = '[B2C-NLBE]'
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    if(memberCenterCheck <0){
        throw new Error(`${titleName} had member center in the past, but it does not have member center now.`)
    }
})

Then("[B2C-NLBE] club-lang must be nl-nl",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-NLBE]'
    const signInSelector = 'body > header > aside.line-1 > aside > nav > div.right-wrapper > div.log_area > a'
    await this.page.waitForSelector(signInSelector)
    await this.page.click(signInSelector)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    await this.page.waitForSelector('#AccountLogin')
    const memberCenterClublangurl = await this.page.url()
    //https://club.benq.eu/ICDS_EU/Home/BenQAuth?system_id=G5&function=Login&lang=de-at&return_url=https%3A%2F%2Fwww.benq.eu%2Fde-at%2Findex.login.html
    const clubExpect = "club.benq.eu"
    const clubActual = memberCenterClublangurl.slice(8,20)
    const clubLangExpect = "lang=nl-nl"
    const clubLangActual = memberCenterClublangurl.slice(71,81)
    const clubActualCheck = memberCenterClublangurl.indexOf(clubExpect)
    const clubLangActualCheck = memberCenterClublangurl.indexOf(clubLangExpect)
    if(clubActualCheck <0 && clubLangActualCheck <0 ){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect} and club-lang is not ${clubLangExpect} , member center URL includes ${clubActual} and club-lang is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubActualCheck<0){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect}, it is ${clubActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubLangActualCheck <0 ){
        throw new Error(`${titleName} club-lang is not ${clubLangExpect}, it is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
})

Then("[B2C-NLBE] B2C-NLBE Page does not have EC Hostname and URL",{timeout: 120 * 5000},async function(){
    const titleName = '[B2C-NLBE]'
    const productInfoUrl = "https://www.benq.eu/nl-be/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
    //console.log(productInfoJson)
    const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].buyNowlink
        const ecCheck = productInfoJson[i].checkProduct
        //console.log(ecUrl)
        if(ecCheck !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
            //因為ENAP有些buyNowlink是us-buy, 官網上沒開EC, 但是Product Json上有
            //https://www.benq.com/en-ap/projector/accessory/carrybag-ql01.html
            //buyNowlink: "https://buy.benq.com/us-buy/soft-carrying-case-for-ht3550-tk850-tk810-tk800m-ht2150st-mh750-mh760-ht2050-ht3050-ht2550-tk800.html"
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-ENNO Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/en-no/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-ENNO] B2B URL must be en-eu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENNO]'
    const b2bNameExpect = 'en-eu'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

Then("[B2C-ENNO] B2C-ENNO Page does not have Member Center",{timeout: 124 * 5000},async function(){
    const titleName = '[B2C-ENNO]'
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    //目前en-ap沒有member center
    //以前沒有member center但現在有的話
    if(memberCenterCheck > 0){
        throw new Error(`${titleName}  did not have member center in the past, but it have member center now.`)
    }
})

Then("[B2C-ENNO] B2C-ENNO Page does not have EC Hostname and URL",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENNO]'
    const productInfoUrl = "https://www.benq.eu/en-no/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
        //console.log(productInfoJson)
    const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].buyNowlink
        const ecCheck = productInfoJson[i].checkProduct
        //console.log(ecUrl)
        if(ecCheck !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
            //因為ENAP有些buyNowlink是us-buy, 官網上沒開EC, 但是Product Json上有
            //https://www.benq.com/en-ap/projector/accessory/carrybag-ql01.html
            //buyNowlink: "https://buy.benq.com/us-buy/soft-carrying-case-for-ht3550-tk850-tk810-tk800m-ht2150st-mh750-mh760-ht2050-ht3050-ht2550-tk800.html"
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-ENFI Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/en-fi/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-ENFI] B2B URL must be en-eu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENFI]'
    const b2bNameExpect = 'en-eu'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

When("[B2C-ENFI] Go to Member Center",{timeout: 12 * 5000},async function(){
    const titleName = '[B2C-ENFI]'
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    if(memberCenterCheck <0){
        throw new Error(`${titleName} had member center in the past, but it does not have member center now.`)
    }
})

Then("[B2C-ENFI] club-lang must be en-fi",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENFI]'
    const signInSelector = 'body > header > aside.line-1 > aside > nav > div.right-wrapper > div.log_area > a'
    await this.page.waitForSelector(signInSelector)
    await this.page.click(signInSelector)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    await this.page.waitForSelector('#AccountLogin')
    const memberCenterClublangurl = await this.page.url()
    // https://club.benq.eu/ICDS_EU/Home/BenQAuth?system_id=G5&function=Login&lang=en-dk&return_url=https%3A%2F%2Fwww.benq.eu%2Fen-dk%2Findex.login.html
    const clubExpect = "club.benq.eu"
    const clubActual = memberCenterClublangurl.slice(8,20)
    const clubLangExpect = "lang=en-fi"
    const clubLangActual = memberCenterClublangurl.slice(71,81)
    const clubActualCheck = memberCenterClublangurl.indexOf(clubExpect)
    const clubLangActualCheck = memberCenterClublangurl.indexOf(clubLangExpect)
    if(clubActualCheck <0 && clubLangActualCheck <0 ){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect} and club-lang is not ${clubLangExpect} , member center URL includes ${clubActual} and club-lang is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubActualCheck<0){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect}, it is ${clubActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubLangActualCheck <0 ){
        throw new Error(`${titleName} club-lang is not ${clubLangExpect}, it is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
})

Then("[B2C-ENFI] EC hostname must be shop.benq.eu && EC URL must be sc-buy",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENFI]'
    const hostnameExpect = "shop.benq.eu"
    const productInfoUrl = "https://www.benq.eu/en-fi/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
    //console.log(productInfoJson)
    //如果以前有EC但現在沒了EC
    const noEC = []
    for(let i =0; i<productInfoJson.length ; i++){
        const noECCheck = productInfoJson[i].checkProduct
        const noEcUrlBuy = ""//以前如果沒EC的話就是空的
        if(noECCheck ==noEcUrlBuy || noECCheck.indexOf("sc-buy")<0){
            noEC.push(noECCheck)
        }
    }
    // console.log("noEC:",noEC)
    if(noEC.length === productInfoJson.length){
        throw new Error(`${titleName}  had EC in the past, but now it did not have EC now.`)
    }
    for(let i =0; i<productInfoJson.length ; i++){
        const ecCheck = productInfoJson[i].checkProduct
        const ecHostName = productInfoJson[i].buyNowlink
        if(ecCheck !== "" && ecHostName.indexOf("-buy")>0 && ecHostName.indexOf(hostnameExpect)<0){
            const hostNameActual = ecHostName.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  EC Host Name is not ${hostnameExpect}, it is ${hostNameActual} now. Here is the URL ${ecHostName}`)
        }
    }
    const ecUrlBuyExpect = "sc-buy"//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecCheck = productInfoJson[i].checkProduct
        const ecUrl= productInfoJson[i].buyNowlink
        //console.log(ecUrl)
        if(ecCheck !== "" && ecCheck.indexOf("-buy")>0 && ecCheck.indexOf(ecUrlBuyExpect)<0){
            const ecUrlBuyActual = ecCheck.slice(21,27) //xx-buy
            throw new Error(`${titleName}  EC Host Name is not ${ecUrlBuyExpect}, it is ${ecUrlBuyActual} now. Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-ENIS Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/en-is/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-ENIS] B2B URL must be en-eu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENIS]'
    const b2bNameExpect = 'en-eu'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

Then("[B2C-ENIS] B2C-ENIS Page does not have Member Center",{timeout: 124 * 5000},async function(){
    const titleName = '[B2C-ENIS]'
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    //目前en-ap沒有member center
    //以前沒有member center但現在有的話
    if(memberCenterCheck > 0){
        throw new Error(`${titleName}  did not have member center in the past, but it have member center now.`)
    }
})

Then("[B2C-ENIS] B2C-ENIS Page does not have EC Hostname and URL",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENIS]'
    const productInfoUrl = "https://www.benq.eu/en-is/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
        //console.log(productInfoJson)
    const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].buyNowlink
        const ecCheck = productInfoJson[i].checkProduct
        //console.log(ecUrl)
        if(ecCheck !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
            //因為ENAP有些buyNowlink是us-buy, 官網上沒開EC, 但是Product Json上有
            //https://www.benq.com/en-ap/projector/accessory/carrybag-ql01.html
            //buyNowlink: "https://buy.benq.com/us-buy/soft-carrying-case-for-ht3550-tk850-tk810-tk800m-ht2150st-mh750-mh760-ht2050-ht3050-ht2550-tk800.html"
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-RURU Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.com/ru-ru/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-RURU] B2B URL must be ru-ru",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-RURU]'
    const b2bNameExpect = 'ru-ru'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.com/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

Then("[B2C-RURU] B2C-RURU Page does not have Member Center",{timeout: 124 * 5000},async function(){
    const titleName = '[B2C-RURU]'
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    //目前en-ap沒有member center
    //以前沒有member center但現在有的話
    if(memberCenterCheck > 0){
        throw new Error(`${titleName}  did not have member center in the past, but it have member center now.`)
    }
})

Then("[B2C-RURU] B2C-RURU Page does not have EC Hostname and URL",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-RURU]'
    const productInfoUrl = "https://www.benq.com/ru-ru/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
        //console.log(productInfoJson)
    const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].buyNowlink
        const ecCheck = productInfoJson[i].checkProduct
        //console.log(ecUrl)
        if(ecCheck !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
            //因為ENAP有些buyNowlink是us-buy, 官網上沒開EC, 但是Product Json上有
            //https://www.benq.com/en-ap/projector/accessory/carrybag-ql01.html
            //buyNowlink: "https://buy.benq.com/us-buy/soft-carrying-case-for-ht3550-tk850-tk810-tk800m-ht2150st-mh750-mh760-ht2050-ht3050-ht2550-tk800.html"
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-PLPL Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/pl-pl/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-PLPL] B2B URL must be pl-pl",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-PLPL]'
    const b2bNameExpect = 'pl-pl'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

When("[B2C-PLPL] Go to Member Center",{timeout: 12 * 5000},async function(){
    const titleName = '[B2C-PLPL]'
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    if(memberCenterCheck <0){
        throw new Error(`${titleName} had member center in the past, but it does not have member center now.`)
    }
})

Then("[B2C-PLPL] club-lang must be en-eu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-PLPL]'
    const signInSelector = 'body > header > aside.line-1 > aside > nav > div.right-wrapper > div.log_area > a'
    await this.page.waitForSelector(signInSelector)
    await this.page.click(signInSelector)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    await this.page.waitForSelector('#AccountLogin')
    const memberCenterClublangurl = await this.page.url()
    // https://club.benq.eu/ICDS_EU/Home/BenQAuth?system_id=G5&function=Login&lang=en-dk&return_url=https%3A%2F%2Fwww.benq.eu%2Fen-dk%2Findex.login.html
    const clubExpect = "club.benq.eu"
    const clubActual = memberCenterClublangurl.slice(8,20)
    const clubLangExpect = "lang=en-eu"
    const clubLangActual = memberCenterClublangurl.slice(71,81)
    const clubActualCheck = memberCenterClublangurl.indexOf(clubExpect)
    const clubLangActualCheck = memberCenterClublangurl.indexOf(clubLangExpect)
    if(clubActualCheck <0 && clubLangActualCheck <0 ){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect} and club-lang is not ${clubLangExpect} , member center URL includes ${clubActual} and club-lang is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubActualCheck<0){    
    throw new Error(`${titleName} member center URL does not include ${clubExpect}, it is ${clubActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
    if(clubLangActualCheck <0 ){
        throw new Error(`${titleName} club-lang is not ${clubLangExpect}, it is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
    }
})

Then("[B2C-PLPL] EC hostname must be shop.benq.eu && EC URL must be eu-buy",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-PLPL]'
    const hostnameExpect = "shop.benq.eu"
    const productInfoUrl = "https://www.benq.eu/pl-pl/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
    //console.log(productInfoJson)
    //如果以前有EC但現在沒了EC
    const noEC = []
    for(let i =0; i<productInfoJson.length ; i++){
        const noECCheck = productInfoJson[i].checkProduct
        const noEcUrlBuy = ""//以前如果沒EC的話就是空的
        if(noECCheck ==noEcUrlBuy || noECCheck.indexOf("eu-buy")<0){
            noEC.push(noECCheck)
        }
    }
    // console.log("noEC:",noEC)
    if(noEC.length === productInfoJson.length){
        throw new Error(`${titleName}  had EC in the past, but now it did not have EC now.`)
    }
    for(let i =0; i<productInfoJson.length ; i++){
        const ecCheck = productInfoJson[i].checkProduct
        const ecHostName = productInfoJson[i].buyNowlink
        if(ecCheck !== "" && ecHostName.indexOf("-buy")>0 && ecHostName.indexOf(hostnameExpect)<0){
            const hostNameActual = ecHostName.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  EC Host Name is not ${hostnameExpect}, it is ${hostNameActual} now. Here is the URL ${ecHostName}`)
        }
    }
    const ecUrlBuyExpect = "eu-buy"//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecCheck = productInfoJson[i].checkProduct
        const ecUrl= productInfoJson[i].buyNowlink
        //console.log(ecUrl)
        if(ecCheck !== "" && ecCheck.indexOf("-buy")>0 && ecCheck.indexOf(ecUrlBuyExpect)<0){
            const ecUrlBuyActual = ecCheck.slice(21,27) //xx-buy
            throw new Error(`${titleName}  EC Host Name is not ${ecUrlBuyExpect}, it is ${ecUrlBuyActual} now. Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-BGBG Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/bg-bg/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-BGBG] B2B URL must be en-eu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-BGBG]'
    const b2bNameExpect = 'en-eu'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

Then("[B2C-BGBG] B2C-BGBG Page does not have Member Center",{timeout: 124 * 5000},async function(){
    const titleName = '[B2C-BGBG]'
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    //目前en-ap沒有member center
    //以前沒有member center但現在有的話
    if(memberCenterCheck > 0){
        throw new Error(`${titleName}  did not have member center in the past, but it have member center now.`)
    }
})

Then("[B2C-BGBG] B2C-BGBG Page does not have EC Hostname and URL",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-BGBG]'
    const productInfoUrl = "https://www.benq.eu/bg-bg/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
        //console.log(productInfoJson)
    const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].buyNowlink
        const ecCheck = productInfoJson[i].checkProduct
        //console.log(ecUrl)
        if(ecCheck !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
            //因為ENAP有些buyNowlink是us-buy, 官網上沒開EC, 但是Product Json上有
            //https://www.benq.com/en-ap/projector/accessory/carrybag-ql01.html
            //buyNowlink: "https://buy.benq.com/us-buy/soft-carrying-case-for-ht3550-tk850-tk810-tk800m-ht2150st-mh750-mh760-ht2050-ht3050-ht2550-tk800.html"
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-ELGR Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/el-gr/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-ELGR] B2B URL must be en-eu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ELGR]'
    const b2bNameExpect = 'en-eu'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

Then("[B2C-ELGR] B2C-ELGR Page does not have Member Center",{timeout: 124 * 5000},async function(){
    const titleName = '[B2C-ELGR]'
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    //目前en-ap沒有member center
    //以前沒有member center但現在有的話
    if(memberCenterCheck > 0){
        throw new Error(`${titleName}  did not have member center in the past, but it have member center now.`)
    }
})

Then("[B2C-ELGR] B2C-ELGR Page does not have EC Hostname and URL",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ELGR]'
    const productInfoUrl = "https://www.benq.eu/el-gr/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
        //console.log(productInfoJson)
    const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].buyNowlink
        const ecCheck = productInfoJson[i].checkProduct
        //console.log(ecUrl)
        if(ecCheck !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
            //因為ENAP有些buyNowlink是us-buy, 官網上沒開EC, 但是Product Json上有
            //https://www.benq.com/en-ap/projector/accessory/carrybag-ql01.html
            //buyNowlink: "https://buy.benq.com/us-buy/soft-carrying-case-for-ht3550-tk850-tk810-tk800m-ht2150st-mh750-mh760-ht2050-ht3050-ht2550-tk800.html"
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-HUHU Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/hu-hu/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-HUHU] B2B URL must be en-eu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-HUHU]'
    const b2bNameExpect = 'en-eu'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

Then("[B2C-HUHU] B2C-HUHU Page does not have Member Center",{timeout: 124 * 5000},async function(){
    const titleName = '[B2C-HUHU]'
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    //目前en-ap沒有member center
    //以前沒有member center但現在有的話
    if(memberCenterCheck > 0){
        throw new Error(`${titleName}  did not have member center in the past, but it have member center now.`)
    }
})

Then("[B2C-HUHU] B2C-HUHU Page does not have EC Hostname and URL",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-HUHU]'
    const productInfoUrl = "https://www.benq.eu/hu-hu/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
        //console.log(productInfoJson)
    const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].buyNowlink
        const ecCheck = productInfoJson[i].checkProduct
        //console.log(ecUrl)
        if(ecCheck !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
            //因為ENAP有些buyNowlink是us-buy, 官網上沒開EC, 但是Product Json上有
            //https://www.benq.com/en-ap/projector/accessory/carrybag-ql01.html
            //buyNowlink: "https://buy.benq.com/us-buy/soft-carrying-case-for-ht3550-tk850-tk810-tk800m-ht2150st-mh750-mh760-ht2050-ht3050-ht2550-tk800.html"
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-LTLT Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/lt-lt/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-LTLT] B2B URL must be en-eu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-LTLT]'
    const b2bNameExpect = 'en-eu'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

Then("[B2C-LTLT] B2C-LTLT Page does not have Member Center",{timeout: 124 * 5000},async function(){
    const titleName = '[B2C-LTLT]'
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    //目前en-ap沒有member center
    //以前沒有member center但現在有的話
    if(memberCenterCheck > 0){
        throw new Error(`${titleName}  did not have member center in the past, but it have member center now.`)
    }
})

Then("[B2C-LTLT] B2C-LTLT Page does not have EC Hostname and URL",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-LTLT]'
    const productInfoUrl = "https://www.benq.eu/lt-lt/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
        //console.log(productInfoJson)
    const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].buyNowlink
        const ecCheck = productInfoJson[i].checkProduct
        //console.log(ecUrl)
        if(ecCheck !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
            //因為ENAP有些buyNowlink是us-buy, 官網上沒開EC, 但是Product Json上有
            //https://www.benq.com/en-ap/projector/accessory/carrybag-ql01.html
            //buyNowlink: "https://buy.benq.com/us-buy/soft-carrying-case-for-ht3550-tk850-tk810-tk800m-ht2150st-mh750-mh760-ht2050-ht3050-ht2550-tk800.html"
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-RORO Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/ro-ro/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-RORO] B2B URL must be en-eu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-RORO]'
    const b2bNameExpect = 'en-eu'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

Then("[B2C-RORO] B2C-RORO Page does not have Member Center",{timeout: 124 * 5000},async function(){
    const titleName = '[B2C-RORO]'
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    //目前en-ap沒有member center
    //以前沒有member center但現在有的話
    if(memberCenterCheck > 0){
        throw new Error(`${titleName}  did not have member center in the past, but it have member center now.`)
    }
})

Then("[B2C-RORO] B2C-RORO Page does not have EC Hostname and URL",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-RORO]'
    const productInfoUrl = "https://www.benq.eu/ro-ro/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
        //console.log(productInfoJson)
    const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].buyNowlink
        const ecCheck = productInfoJson[i].checkProduct
        //console.log(ecUrl)
        if(ecCheck !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
            //因為ENAP有些buyNowlink是us-buy, 官網上沒開EC, 但是Product Json上有
            //https://www.benq.com/en-ap/projector/accessory/carrybag-ql01.html
            //buyNowlink: "https://buy.benq.com/us-buy/soft-carrying-case-for-ht3550-tk850-tk810-tk800m-ht2150st-mh750-mh760-ht2050-ht3050-ht2550-tk800.html"
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-SKSK Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/sk-sk/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-SKSK] B2B URL must be en-eu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-SKSK]'
    const b2bNameExpect = 'en-eu'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

Then("[B2C-SKSK] B2C-SKSK Page does not have Member Center",{timeout: 124 * 5000},async function(){
    const titleName = '[B2C-SKSK]'
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    //目前en-ap沒有member center
    //以前沒有member center但現在有的話
    if(memberCenterCheck > 0){
        throw new Error(`${titleName}  did not have member center in the past, but it have member center now.`)
    }
})

Then("[B2C-SKSK] B2C-SKSK Page does not have EC Hostname and URL",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-SKSK]'
    const productInfoUrl = "https://www.benq.eu/sk-sk/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
        //console.log(productInfoJson)
    const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].buyNowlink
        const ecCheck = productInfoJson[i].checkProduct
        //console.log(ecUrl)
        if(ecCheck !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
            //因為ENAP有些buyNowlink是us-buy, 官網上沒開EC, 但是Product Json上有
            //https://www.benq.com/en-ap/projector/accessory/carrybag-ql01.html
            //buyNowlink: "https://buy.benq.com/us-buy/soft-carrying-case-for-ht3550-tk850-tk810-tk800m-ht2150st-mh750-mh760-ht2050-ht3050-ht2550-tk800.html"
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-UKUA Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/uk-ua/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-UKUA] B2B URL must be en-eu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-UKUA]'
    const b2bNameExpect = 'en-eu'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

Then("[B2C-UKUA] B2C-UKUA Page does not have Member Center",{timeout: 124 * 5000},async function(){
    const titleName = '[B2C-UKUA]'
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    //目前en-ap沒有member center
    //以前沒有member center但現在有的話
    if(memberCenterCheck > 0){
        throw new Error(`${titleName}  did not have member center in the past, but it have member center now.`)
    }
})

Then("[B2C-UKUA] B2C-UKUA Page does not have EC Hostname and URL",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-UKUA]'
    const productInfoUrl = "https://www.benq.eu/uk-ua/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
        //console.log(productInfoJson)
    const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].buyNowlink
        const ecCheck = productInfoJson[i].checkProduct
        //console.log(ecUrl)
        if(ecCheck !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
            //因為ENAP有些buyNowlink是us-buy, 官網上沒開EC, 但是Product Json上有
            //https://www.benq.com/en-ap/projector/accessory/carrybag-ql01.html
            //buyNowlink: "https://buy.benq.com/us-buy/soft-carrying-case-for-ht3550-tk850-tk810-tk800m-ht2150st-mh750-mh760-ht2050-ht3050-ht2550-tk800.html"
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-ENLV Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/en-lv/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-ENLV] B2B URL must be en-eu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENLV]'
    const b2bNameExpect = 'en-eu'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

Then("[B2C-ENLV] B2C-ENLV Page does not have Member Center",{timeout: 124 * 5000},async function(){
    const titleName = '[B2C-ENLV]'
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    //目前en-ap沒有member center
    //以前沒有member center但現在有的話
    if(memberCenterCheck > 0){
        throw new Error(`${titleName}  did not have member center in the past, but it have member center now.`)
    }
})

Then("[B2C-ENLV] B2C-ENLV Page does not have EC Hostname and URL",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENLV]'
    const productInfoUrl = "https://www.benq.eu/en-lv/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
        //console.log(productInfoJson)
    const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].buyNowlink
        const ecCheck = productInfoJson[i].checkProduct
        //console.log(ecUrl)
        if(ecCheck !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
            //因為ENAP有些buyNowlink是us-buy, 官網上沒開EC, 但是Product Json上有
            //https://www.benq.com/en-ap/projector/accessory/carrybag-ql01.html
            //buyNowlink: "https://buy.benq.com/us-buy/soft-carrying-case-for-ht3550-tk850-tk810-tk800m-ht2150st-mh750-mh760-ht2050-ht3050-ht2550-tk800.html"
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-ENRS Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/en-rs/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-ENRS] B2B URL must be en-eu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENRS]'
    const b2bNameExpect = 'en-eu'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

Then("[B2C-ENRS] B2C-ENRS Page does not have Member Center",{timeout: 124 * 5000},async function(){
    const titleName = '[B2C-ENRS]'
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    //目前en-ap沒有member center
    //以前沒有member center但現在有的話
    if(memberCenterCheck > 0){
        throw new Error(`${titleName}  did not have member center in the past, but it have member center now.`)
    }
})

Then("[B2C-ENRS] B2C-ENRS Page does not have EC Hostname and URL",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENRS]'
    const productInfoUrl = "https://www.benq.eu/en-rs/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
        //console.log(productInfoJson)
    const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].buyNowlink
        const ecCheck = productInfoJson[i].checkProduct
        //console.log(ecUrl)
        if(ecCheck !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
            //因為ENAP有些buyNowlink是us-buy, 官網上沒開EC, 但是Product Json上有
            //https://www.benq.com/en-ap/projector/accessory/carrybag-ql01.html
            //buyNowlink: "https://buy.benq.com/us-buy/soft-carrying-case-for-ht3550-tk850-tk810-tk800m-ht2150st-mh750-mh760-ht2050-ht3050-ht2550-tk800.html"
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-ENSI Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/en-si/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-ENSI] B2B URL must be en-eu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENSI]'
    const b2bNameExpect = 'en-eu'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

Then("[B2C-ENSI] B2C-ENSI Page does not have Member Center",{timeout: 124 * 5000},async function(){
    const titleName = '[B2C-ENSI]'
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    //目前en-ap沒有member center
    //以前沒有member center但現在有的話
    if(memberCenterCheck > 0){
        throw new Error(`${titleName}  did not have member center in the past, but it have member center now.`)
    }
})

Then("[B2C-ENSI] B2C-ENSI Page does not have EC Hostname and URL",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENSI]'
    const productInfoUrl = "https://www.benq.eu/en-si/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
        //console.log(productInfoJson)
    const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].buyNowlink
        const ecCheck = productInfoJson[i].checkProduct
        //console.log(ecUrl)
        if(ecCheck !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
            //因為ENAP有些buyNowlink是us-buy, 官網上沒開EC, 但是Product Json上有
            //https://www.benq.com/en-ap/projector/accessory/carrybag-ql01.html
            //buyNowlink: "https://buy.benq.com/us-buy/soft-carrying-case-for-ht3550-tk850-tk810-tk800m-ht2150st-mh750-mh760-ht2050-ht3050-ht2550-tk800.html"
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-ENBA Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/en-ba/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-ENBA] B2B URL must be en-eu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENBA]'
    const b2bNameExpect = 'en-eu'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

Then("[B2C-ENBA] B2C-ENBA Page does not have Member Center",{timeout: 124 * 5000},async function(){
    const titleName = '[B2C-ENBA]'
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    //目前en-ap沒有member center
    //以前沒有member center但現在有的話
    if(memberCenterCheck > 0){
        throw new Error(`${titleName}  did not have member center in the past, but it have member center now.`)
    }
})

Then("[B2C-ENBA] B2C-ENBA Page does not have EC Hostname and URL",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENBA]'
    const productInfoUrl = "https://www.benq.eu/en-ba/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
        //console.log(productInfoJson)
    const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].buyNowlink
        const ecCheck = productInfoJson[i].checkProduct
        //console.log(ecUrl)
        if(ecCheck !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
            //因為ENAP有些buyNowlink是us-buy, 官網上沒開EC, 但是Product Json上有
            //https://www.benq.com/en-ap/projector/accessory/carrybag-ql01.html
            //buyNowlink: "https://buy.benq.com/us-buy/soft-carrying-case-for-ht3550-tk850-tk810-tk800m-ht2150st-mh750-mh760-ht2050-ht3050-ht2550-tk800.html"
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-ENCY Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/en-cy/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-ENCY] B2B URL must be en-eu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENCY]'
    const b2bNameExpect = 'en-eu'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

Then("[B2C-ENCY] B2C-ENCY Page does not have Member Center",{timeout: 124 * 5000},async function(){
    const titleName = '[B2C-ENCY]'
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    //目前en-ap沒有member center
    //以前沒有member center但現在有的話
    if(memberCenterCheck > 0){
        throw new Error(`${titleName}  did not have member center in the past, but it have member center now.`)
    }
})

Then("[B2C-ENCY] B2C-ENCY Page does not have EC Hostname and URL",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENCY]'
    const productInfoUrl = "https://www.benq.eu/en-cy/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
        //console.log(productInfoJson)
    const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].buyNowlink
        const ecCheck = productInfoJson[i].checkProduct
        //console.log(ecUrl)
        if(ecCheck !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
            //因為ENAP有些buyNowlink是us-buy, 官網上沒開EC, 但是Product Json上有
            //https://www.benq.com/en-ap/projector/accessory/carrybag-ql01.html
            //buyNowlink: "https://buy.benq.com/us-buy/soft-carrying-case-for-ht3550-tk850-tk810-tk800m-ht2150st-mh750-mh760-ht2050-ht3050-ht2550-tk800.html"
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-ENEE Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/en-ee/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-ENEE] B2B URL must be en-eu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENEE]'
    const b2bNameExpect = 'en-eu'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

Then("[B2C-ENEE] B2C-ENEE Page does not have Member Center",{timeout: 124 * 5000},async function(){
    const titleName = '[B2C-ENEE]'
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    //目前en-ap沒有member center
    //以前沒有member center但現在有的話
    if(memberCenterCheck > 0){
        throw new Error(`${titleName}  did not have member center in the past, but it have member center now.`)
    }
})

Then("[B2C-ENEE] B2C-ENEE Page does not have EC Hostname and URL",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENEE]'
    const productInfoUrl = "https://www.benq.eu/en-ee/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
        //console.log(productInfoJson)
    const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].buyNowlink
        const ecCheck = productInfoJson[i].checkProduct
        //console.log(ecUrl)
        if(ecCheck !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
            //因為ENAP有些buyNowlink是us-buy, 官網上沒開EC, 但是Product Json上有
            //https://www.benq.com/en-ap/projector/accessory/carrybag-ql01.html
            //buyNowlink: "https://buy.benq.com/us-buy/soft-carrying-case-for-ht3550-tk850-tk810-tk800m-ht2150st-mh750-mh760-ht2050-ht3050-ht2550-tk800.html"
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-ENHR Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/en-hr/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-ENHR] B2B URL must be en-eu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENHR]'
    const b2bNameExpect = 'en-eu'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

Then("[B2C-ENHR] B2C-ENHR Page does not have Member Center",{timeout: 124 * 5000},async function(){
    const titleName = '[B2C-ENHR]'
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    //目前en-ap沒有member center
    //以前沒有member center但現在有的話
    if(memberCenterCheck > 0){
        throw new Error(`${titleName}  did not have member center in the past, but it have member center now.`)
    }
})

Then("[B2C-ENHR] B2C-ENHR Page does not have EC Hostname and URL",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENHR]'
    const productInfoUrl = "https://www.benq.eu/en-hr/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
        //console.log(productInfoJson)
    const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].buyNowlink
        const ecCheck = productInfoJson[i].checkProduct
        //console.log(ecUrl)
        if(ecCheck !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
            //因為ENAP有些buyNowlink是us-buy, 官網上沒開EC, 但是Product Json上有
            //https://www.benq.com/en-ap/projector/accessory/carrybag-ql01.html
            //buyNowlink: "https://buy.benq.com/us-buy/soft-carrying-case-for-ht3550-tk850-tk810-tk800m-ht2150st-mh750-mh760-ht2050-ht3050-ht2550-tk800.html"
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-ENMK Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/en-mk/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-ENMK] B2B URL must be en-eu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENMK]'
    const b2bNameExpect = 'en-eu'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

Then("[B2C-ENMK] B2C-ENMK Page does not have Member Center",{timeout: 124 * 5000},async function(){
    const titleName = '[B2C-ENMK]'
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    //目前en-ap沒有member center
    //以前沒有member center但現在有的話
    if(memberCenterCheck > 0){
        throw new Error(`${titleName}  did not have member center in the past, but it have member center now.`)
    }
})

Then("[B2C-ENMK] B2C-ENMK Page does not have EC Hostname and URL",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENMK]'
    const productInfoUrl = "https://www.benq.eu/en-mk/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
        //console.log(productInfoJson)
    const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].buyNowlink
        const ecCheck = productInfoJson[i].checkProduct
        //console.log(ecUrl)
        if(ecCheck !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
            //因為ENAP有些buyNowlink是us-buy, 官網上沒開EC, 但是Product Json上有
            //https://www.benq.com/en-ap/projector/accessory/carrybag-ql01.html
            //buyNowlink: "https://buy.benq.com/us-buy/soft-carrying-case-for-ht3550-tk850-tk810-tk800m-ht2150st-mh750-mh760-ht2050-ht3050-ht2550-tk800.html"
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
    }
})

Given("Go to B2C-ENMT Page",{timeout: 24 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/en-mt/index.html'+cicGA)
    await this.page.waitForTimeout(10000)//等待10000毫秒
    //#btn_close
    //const closeCookie = 'body > div.modals-wrapper > aside.modal-popup.benq_cookiebar_modal.modal-slide._inner-scroll._show > div.modal-inner-wrap > footer > button.close-button'
    const closeCookie = '#btn_close'
    await this.page.waitForSelector(closeCookie)
    await this.page.click(closeCookie)
    await this.page.waitForTimeout(5000)//等待5000毫秒
})

Then("[B2C-ENMT] B2B URL must be en-eu",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENMT]'
    const b2bNameExpect = 'en-eu'//以前的結果
    const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
    const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
    // 取得name
    const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
    const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
    const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
    if(b2bURLCheck < 0 ){
        throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${b2bURL}`)
    }
})

Then("[B2C-ENMT] B2C-ENMT Page does not have Member Center",{timeout: 124 * 5000},async function(){
    const titleName = '[B2C-ENMT]'
    await this.page.waitForTimeout(5000)//等待5000毫秒
    const pageHeader = 'body > header'
    const headerInnerHtml = await this.page.$eval(pageHeader, element => element.innerHTML);
    const memberCenterCheck = headerInnerHtml.indexOf("log_area")
    //目前en-ap沒有member center
    //以前沒有member center但現在有的話
    if(memberCenterCheck > 0){
        throw new Error(`${titleName}  did not have member center in the past, but it have member center now.`)
    }
})

Then("[B2C-ENMT] B2C-ENMT Page does not have EC Hostname and URL",{timeout: 24 * 5000},async function(){
    const titleName = '[B2C-ENMT]'
    const productInfoUrl = "https://www.benq.eu/en-mt/jcr:content.productinfo.json"
    const productInfo = await request.get(productInfoUrl)
    const productInfoJson = JSON.parse(productInfo)
        //console.log(productInfoJson)
    const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
    for(let i =0; i<productInfoJson.length ; i++){
        const ecUrl = productInfoJson[i].buyNowlink
        const ecCheck = productInfoJson[i].checkProduct
        //console.log(ecUrl)
        if(ecCheck !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
            //因為ENAP有些buyNowlink是us-buy, 官網上沒開EC, 但是Product Json上有
            //https://www.benq.com/en-ap/projector/accessory/carrybag-ql01.html
            //buyNowlink: "https://buy.benq.com/us-buy/soft-carrying-case-for-ht3550-tk850-tk810-tk800m-ht2150st-mh750-mh760-ht2050-ht3050-ht2550-tk800.html"
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
    }
})
const puppeteer = require('puppeteer');
const expect = require('chai').expect;

// const {click,getText,getCount,shouldNotExist,waitForText} = require('../lib/helper')

describe('[B2C-ENUS] pd2700u',()=>{
    let browser
    let page
    //Test Hooks:before, beforeEach, after, afterEach
    //before:每個test case執行之前先做的動作(page/browswer)
    before(async function(){
        browser=await puppeteer.launch({
            executablePath:
            "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
            headless:false,//有無需要開視窗,false要開,true不開
            //slowMo:100,// slow down by 100ms
            devtools:false//有無需要開啟開發人員工具
        })
        page=await browser.newPage()
        await page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
        await page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    })
    //after:每個test case執行之後統一要做的動作(page/browswer)
    after(async function(){
        await browser.close()
    })
    // beforeEach(async function(){
    //     browser=await puppeteer.launch({
    //         executablePath:
    //         "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
    //         headless:false,//有無需要開視窗,false要開,true不開
    //         slowMo:100,// slow down by 100ms
    //         devtools:false//有無需要開啟開發人員工具
    //     })
    //     page=await browser.newPage()
    //     await page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
    //     await page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    // })
    // afterEach(async function(){
    //     // Runs after each test steps(所有test step執行完的步驟)
    //     await browser.close()
    // })
    it('新增webpage',async function(){
        //舉例總部上架https://www.benq.com/en-us/monitor/photographer/sw321c.html
        //希望檢測 2021/11/30之前發布 https://www.benq.com/zh-tw/monitor/photographer/sw321c.html 
        //希望檢測 2021/12/31之前所有國家https://www.benq.com/xx-xx/monitor/photographer/sw321c.html 全部發布
        await page.setViewport({width:1200,height:1000})
        const response = await page.goto('https://www.benq.com/en-us/monitor/designer/pd2700u.html')
        console.log('Status Code:',response.status())
        expect(response.status()).equal(200)
    })
    it('新增webpage',async function(){
        //舉例總部上架https://www.benq.com/en-us/monitor/photographer/sw321c.html
        //希望檢測 2021/11/30之前發布 https://www.benq.com/zh-tw/monitor/photographer/sw321c.html 
        //希望檢測 2021/12/31之前所有國家https://www.benq.com/xx-xx/monitor/photographer/sw321c.html 全部發布
        await page.setViewport({width:1200,height:1000})
        const response = await page.goto('https://www.benq.com/en-us/monitor/designer/pd2700.html')
        // await page.on('response', response => {expect(response.status).toEqual(400)})
        console.log('Status Code:',response.status())
        expect(response.status()).equal(404)
    })
    
})